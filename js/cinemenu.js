/**
 * Created by xdire on 12.02.15.
 */

var cSystemMenuData=[];
var cSystemMenuElements=[];
var cSystemMenuCurrent=null;
var cSystemMenuContainer=null;
var cSystemMenuLoadTry=0;

(function(){

    var $cMenu = function(){return new cMenu;};

    var defaultMenu = [
        {id:"findresto",active:true,name:"Find Restaurants",href:"index.html",event:false,notify:false,notifies:0},
        {id:"shopcart",active:false,name:"My order",href:"#",event:$cine().action.cartOpen,notify:false,notifies:0},
        {id:"orderstat",active:false,name:"My order",href:"order.html",event:false,notify:false,notifies:0},
        {id:"useraccount",active:true,name:"My Account",href:"user.html",event:false,notify:false,notifies:0},
        {id:"about",active:true,name:"About Voilà and Contact",href:"about.html",event:false,notify:false,notifies:0}
    ];

    var cMenu = function(){

        var i=0,k=0,l=0,c;
        var menuMainClass='nav navbar-nav';
        var menuElemClass='dropdown megamenu-fullwidth';

        if(cSystemMenuData.length==0){
            l=defaultMenu.length;
            cSystemMenuData = defaultMenu.slice(0);
            for(i=0;i<l;i++){
                c=defaultMenu[i];
                cSystemMenuElements[i]=c.id;
            }

            i=l=0;
            c=null;
        }

        this.init = function(container){

            var cont=document.getElementById(container);
            var intv;

            if(cont!=null){
                cSystemMenuContainer=cont;
            } else {
                intv=setInterval(function(){
                    var cont=document.getElementById(container);
                    if(cont!=null){
                        clearInterval(intv);
                        cSystemMenuContainer=cont;
                        renderMenu();
                    }
                    cSystemMenuLoadTry++;
                    if(cSystemMenuLoadTry==1000){
                        clearInterval(intv);
                        alert("Application encounter errors during loading please reload app");
                    }
                },200);
            }

        };

        this.disable = function(element){

            var elem;
            if(elem = cSystemMenuElements.indexElm(element)){
                c=cSystemMenuData[elem.pos];
                c.active=false;
                renderMenu();
            }

        };

        this.enable = function(element){

            var elem;
            if(elem = cSystemMenuElements.indexElm(element)){
                c=cSystemMenuData[elem.pos];
                c.active=true;
                renderMenu();
            }

        };

        this.load = function(){
            return renderMenu();
        };

        function renderMenu(){

            var i=0,l=0,c,result;
            l=cSystemMenuData.length;

            result=document.createElement('ul');
            result.className=menuMainClass;

            for(i=0;i<l;i++){

                c=cSystemMenuData[i];
                if(c.active) {

                    li = document.createElement('li');
                    li.className = menuElemClass;
                    an = document.createElement('a');
                    an.className = 'dropdown-toggle';
                    an.innerHTML = c.name;
                    an.setAttribute('href', c.href);
                    if (c.event) {
                        if (typeof c.event === 'function') {
                            an.addEventListener('click', function (e) {
                                this(e);
                                unCollapse();
                            }.bind(c.event));
                        }
                    }

                    li.appendChild(an);
                    result.appendChild(li);

                }
            }

            if(cSystemMenuContainer!=null){
                cineFnClearDOM(cSystemMenuContainer);
                cSystemMenuContainer.appendChild(result);
            }

            return result;
        }

        function cineFnClearDOM(node){
            if(node!=null){
                while (node.firstChild){
                    node.removeChild(node.firstChild);
                }
            }
        }

    };

    if(!window.$cmenu){window.$cmenu = $cMenu();}
})();