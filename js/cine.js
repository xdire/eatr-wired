/**
 * VOILA Main Query Library
 * Created by xdire on 29.09.14.
 * Modified 11.02.2015
 */

var cineDevice=null;
var cineCurrentView=null;

// System window variables
var cineViewO=0;
var cineViewW=0;
var cineViewH=0;
var cineTOS=false;
var cineMENUBGR=null;

// System variables
var cineFnMarkersObjects=null;
var cineFnMapObject=null;
var cineFnMapObjectLayer=null;
var cineFnLastCoord=null;
var cineFnLastCoordTime=null;
var cineFnNotify=null;
var cineFnWindows=[];
var cineFnLastWindow;
var cineFnCurrentWindow;
var cineFnCurrentModal=null;
var cineFnIterator=null;
var cineFnConnIterator=null;
var cineFnLastCoords=0;
var cineFnPendingReq=[];

// Main screen active orders variables
var cFscrCont=null;
var cFscrOrd=null;
var cFscrOrdBox=null;
var cFscrBox=null;
var cFscrOrdW=0;

var CRActiveRow=null;
var CRActiveID=null;

// Main screen controls variables
var cFscrHeader=null;
var cFscrRbutton=null;
var cFscrRlabel=null;
var cFscrTlabel=null;
var cFscrMbutton=null;
var cFscrRestaurant=null;
var cFscrReservation=null;

var cineFnOrderList=null;
var cineFnRestoList=null;
var cineFnRestoType=null;
var cineFnTimeObject=null;
var cineFnCartStatus;
var cineFnCartObject=null;
var cineFnRestoTitle;
var cineFnMenuArray;
var cineFnSidesArray;
var cineFnOrderObject=null;

var cOrderPrice={priceCash:0,taxCash:0,tipCash:0,discCash:0,totalCash:0,tipPercent:0,taxPercent:0,discValue:0,discType:0};

var cinePriceObject;
var cinePromoCodeObject;
var cinePriceTitle;
var cineSideOptions;

var cineDomMutableOne;
var cineRestoState=['Created','Pushed to restaurant','Viewed by Restaurant','Rejected by Restaurant','Accepted by Restaurant','Meals Are Cooking','Payment pending','Payment Pending','Paid and Feed','Closed'];
var cineRestoTypes=[
    'American (Traditional)', 'American (New)', 'Italian', 'Latin American', 'Thai', 'Pizza',
    'Burgers', 'Mexican', 'Chinese', 'Asian Fusion', 'Greek', 'Mediterranean', 'Sushi',
    'Seafood', 'Steakhouse', 'Middle Eastern', 'Bistro', 'Gastropubs', 'French',
    'Breakfast & Brunch', 'Asian', 'Tapas/Small Plates', 'Bars', 'Irish', 'Pubs',
    'European', 'Indian', 'Japanese', 'Turkish', 'Cafes/Sandwiches'];

var astype={number:'num',string:'str'};

(function(){
    var $cine = function(data){return new cineBox(data);};
    var $cineFnDeviceType=false;
    var $cineFnMainObjects={
        //queryBase:'http://eatnow.xdire.ru/srv',queryApi:'srv',
//        queryBase:'http://eatnow1212/srv',queryApi:'srv',
        queryBase:'https://api.myvoila.me/srv',queryApi:'srv',
        queryResto:'restoinfo',queryOrder:'restoorder',
        methodNavi:'get_location',methodUser:'userinfo',methodCard:'cardinfo',methodForm:'cardform',methodList:'getrestolist',methodCartId:'getorderident',
        pageAnims:'cine_fn_animations',
        pageSearch:'cine_fn_form_search',pageSearchInp:'cine_fn_loc_search',
        modalDefW:320,modalDefH:64,modalClass:'cine_fn_modal_default',modalHeader:'h3',
        domLayer:'div',domButton:'a',domButtonForm:'input',
        domForm:'form',domInput:'input', domSelect:'select',
        formCenter:'cine_fn_form_centerer',
        btnClass:'cine_fn_simple_btn',mbtnClass:'cine_fn_color_btn',
        reqPrefix:'cinereq_',infoPrefix:'cineinfo_',
        bTreeLink:'https://js.braintreegateway.com/v2/braintree.js'};

    var cineBox = function(ciobject){

        this.version='2.05';
        var mainIterObj=null;
        var mainIterCout=0;

// LOADING SECTION -----------------------------------------------------
        function cineBoxFnAppLoad(state,init){

            if(state){

                cineViewH=window.innerHeight;
                cineViewW=window.innerWidth;
                cFscrOrdW=(cineViewW/100)*90;

                if(init !== null)
                {
                    //var cine = new cineBox();
                    //var cine = $cine();
                    for(var i in init)
                    {

                        var c=init[i];
                        if(c=='list')
                            $cine().nav.navCheck(0);
                        //if(c=='nav')
                        //    cine.nav.navCheck(1);
                        if(c=='page')
                            $cine().init.page();
                        if(c=='cart')
                            $cine().init.cart();
                        if(c=='order')
                            $cine().init.order();

                    }
                }
            }
        }

        this.load = {
            sysChckTime:100,
            sysInitWith:null,
            sysChck: function(){
                mainIterCout++;
                if(mainIterCout>1000)
                    clearInterval(mainIterObj);
                if(document.readyState==="complete"){
                    clearInterval(mainIterObj);
                    cineBoxFnAppLoad(true,this.sysInitWith);
                }
            },
            sysInit: function(initwith){
                if(typeof initwith === 'string')
                    this.sysInitWith=initwith.split(',');
                mainIterObj=setInterval(this.sysChck.bind(this),this.sysChckTime);
            }
        };

// INITIALIZATION SECTION -----------------------------------------------------
        this.init = {

            page: function(){
                $cineFnDeviceType=true;
                cineIndexAfterLoad();
            },
            cart: function(){

                var item='{"order":0}';

                var user=0;
                var userkey='';
                if(window.localStorage!=='undefined'){
                    item=window.localStorage.getItem('userorder');
                    user=window.localStorage.getItem('user_id');
                    userkey=window.localStorage.getItem('user_key');
                    if(user==null){
                        user=0;
                        userkey='';
                    }
                }

                XWF.connQueryObject = {query:"restoorder", type:"getorderident"};
                XWF.connEarlyTermination=false;
                XWF.connQueryData = {
                    ident:item,
                    user:user,
                    userkey:userkey
                };
                XWF.send({
                    raiseWindow:false,
                    onComplete:cineBoxFnOrderCartLoad
                });


            },
            order: function(){
                var item='{"order":0}';
                //var url=$cineFnMainObjects.queryBase+'?query='+$cineFnMainObjects.queryOrder+'&type='+$cineFnMainObjects.methodCartId;
                //var cine = new cineBox();
                var user=0;
                var userkey='';

                if(window.localStorage!=='undefined'){
                    item=window.localStorage.getItem('userorder');
                    user=window.localStorage.getItem('user_id');
                    userkey=window.localStorage.getItem('user_key');
                    if(user==null){
                        user=0;
                        userkey='';
                    }

                    XWF.connQueryObject = {query:"restoorder", type:"getorderident"};
                    XWF.connEarlyTermination=false;
                    XWF.connQueryData = {
                        ident:item,
                        user:user,
                        userkey:userkey
                    };
                    XWF.send({
                        raiseWindow:false,
                        onComplete:cineBoxFnOrderCartLoadSilent
                    });
                }

            }
        };

// AJAX SECTION -----------------------------------------------------
        /*
        this.ajaxfunc = {

            initObj: function(){
                var xhr = false;
                if (window.XMLHttpRequest){
                    xhr = new XMLHttpRequest();
                }return xhr;
            },
            postData: function(xhr,url,data,callback,backobj){
                postData(xhr,url,data,callback,false);
            }
        };

        function postData(xhr,url,data,callback,ival){
            xhr.open('POST',url,true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            xhr.onreadystatechange = function(){
                if (xhr.readyState == 4 && xhr.status == 200){
                    if(xhr.status == 200){
                        clearInterval(ival);
                        cineFnConnIterator=null;
                        callback(xhr.responseText);
                    }
                    else {
                        if (xhr.status === 404) {
                            callback('{"notify":true,"notify_msg":"Requested data cannot be found","notify_time":"5"}');
                        } else if (xhr.status === 500) {
                            callback('{"notify":true,"notify_msg":"Requested data encounter server error","notify_time":"5"}');
                        } else {
                            callback('{"notify":true,"notify_msg":"Connection to server is unavailable","notify_time":"5"}');
                        }
                    }
                }
            };

            if(ival==false){
                xhr.onerror = function () {
                    if (cineFnConnIterator == null)
                        cineNotify({message: 'Internet connection lost, you can resume after it will restore'});
                    delete xhr;
                    var nxhr = new XMLHttpRequest();
                    var inval;
                    if(!cineFnPendingReq.indexElm(url)) {
                        inval = setInterval(function(){
                            postData(nxhr,url,data,callback,inval);
                        },5000);
                        cineFnPendingReq.push(url);
                    }
                };
            }

            if(typeof data==='object')
                data = cineBoxFnObjToPost(data,'ajax');
            xhr.send(data);
        }
        */
// NAVIGATION SECTION -----------------------------------------------------
        this.nav = {

            navCheck: function(type){
                if (navigator.geolocation){
                    if(type==0){
                        navigator.geolocation.getCurrentPosition(this.navGetList,function(e){
                            var code = e.code;
                            if(code==3){
                                cineNotify({message:'Location coordinates did not ready, try again later',destroy:5});
                            } else {
                                cineNotify({message:'Navigation is turned off or current location is not available',destroy:5});
                            }
                            this.navGetList(false);
                        }.bind(this),{maximumAge:0,timeout:10000});
                    }
                    //if(type==1)
                        //navigator.geolocation.getCurrentPosition(this.navShowPos);
                    return true;
                } else {
                    cineNotify({message:'Navigation is turned off on your phone, you need to turn it on in your settings manually',destroy:5});
                }
                return false;
            },

            /*navShowPos: function(pos){
                var url;
                console.log('LATITUDE: '+ pos.coords.latitude);
                console.log('LONGITUDE: ' + pos.coords.longitude);
                if(url = $cineFnMainObjects.queryBase){
                    url += '?query='+$cineFnMainObjects.methodNavi;
                    var cine = new cineBox();
                    //40.577505, -73.968848
                    var lat = pos.coords.latitude;
                    var lng = pos.coords.longitude;
                    //40.853164, -73.968934
                    //var lat = 40.577505;
                    //var lng = -73.968848;
                    //var lat = 40.853164;
                    //var lng = -73.968934;
                    cine.ajax('post',url,{'lat':lat,'long':lng},function(msg){
                        var cine = new cineBox();
                        cine.nav.navPopForm(msg);
                    });
                }
            },

            navPopForm: function(data){
                if(fid = document.getElementById('cine_fn_form_search')){

                    fld = document.getElementById('cine_fn_loc_search');
                    var newel = document.createElement('p');
                    var r = JSON.parse(data);
                    r=cineBoxParseGeoQuery(r);

                    if(r){
                        var loc={subcity:false,city:false};
                        subrb= r.sublocality_level_1;
                        lclty= r.locality;
                        adara= r.administrative_area_level_1;

                        if(subrb){
                            loc['subcity']=subrb;
                            if(lclty!=adara){
                                loc['city']=adara;
                            } else {
                                loc['city']=lclty;
                            }
                        } else {
                            loc['subcity']= lclty;
                            loc['city']= adara;
                        }

                        fld.value=cineBoxObjToCommaString(loc);
                        newel.innerHTML = '<div style="display:inline-block; width:320px; text-align:left;">CITY:'+loc.city+' DISTRICT '+loc.subcity+'</div>';
                        fid.parentNode.insertBefore(newel,fid.nextSibling);
                    }

                }
            },*/
            navGetList: function(pos)
            {
                var lat,lng;
                //console.log('LATITUDE: '+ pos.coords.latitude);
                //console.log('LONGITUDE: ' + pos.coords.longitude);
                //pos=false;
                try
                {
                    lat = 0;
                    lng = 0;

                    if(!pos){
                        pos={coords:{latitude:lat,longitude:lng}};
                    } else {
                        lat = pos.coords.latitude;
                        lng = pos.coords.longitude;
                        var time=new Date();
                        cineFnLastCoords=(time.getTime()/1000);
                    }

                    if(lat!=0 && lng!=0){
                        cineFnLastCoord={lat:lat,lng:lng};
                    }
                    //console.log('BEGIN QUERY FOR RESTAURANTS');
                    //console.log(cineFnTimeObject);

                    if(cineFnTimeObject!=null)
                    {

                        var odate=cineFnTimeObject.year+'-'+cineFnTimeObject.month+'-'+cineFnTimeObject.day+' '+
                            cineFnTimeObject.hour+':'+cineFnTimeObject.minute+':00';
                        var odateT=cineFnTimeObject.year+'-'+cineFnTimeObject.month+'-'+cineFnTimeObject.day+'T'+
                            cineFnTimeObject.hour+':'+cineFnTimeObject.minute+':00';

                        cineFnLastCoordTime=odate;

                        var jdate = new Date(odateT);
                        var udate = jdate.getUTCFullYear()+'-'+jdate.getUTCMonth()+'-'+jdate.getUTCDate()+' '+jdate.getUTCHours()+':'+jdate.getUTCMinutes()+':00';
                        var utz = jdate.getTimezoneOffset();
                        var dayOfWeek = jdate.getUTCDay();
                        var timeStamp = jdate.getTime();

                        //console.log(utz);

                        XWF.connQueryObject = {query: "restoinfo", type: "getrestolist"};
                        XWF.connEarlyTermination = false;
                        XWF.connQueryData = {
                            ident: 0, lat: lat, long: lng, user: 0, date:odate, userdate:udate, usertz:utz, dayofweek:dayOfWeek, timestamp: timeStamp
                        };
                        XWF.send({
                            raiseWindow: false,
                            onComplete: cineBoxFnObjRestoList,
                            returnObject: pos
                        });

                    }

                } catch (err){
                    alert(err.message);
                }

            }
        };

        this.resourse = {
            // inline = display inline
            // name = name of the button
            // submit = define type submit
            // close = object linked to manipultaion for close
            // func = linked function to execute
            // method = CLICK / MOUSEDOWN etc
            // id = if not defined, using random one
            button: function(inline,name,submit,close,func,method,id,viewobj,defer,deferfunc){
                var b,tid;
                //var c = new cineBox();
                if(inline){
                    b = document.createElement($cineFnMainObjects.domButtonForm);
                    b.className = 'cine_fn_color_btn inline';
                    b.setAttribute('cinerootobject',viewobj);

                    if(id !== 'undefined' || id.length <1 ){
                        tid = new Date().getTime();
                        b.setAttribute('id','cine_color_btn'+tid);
                    }

                    if(submit)
                        b.setAttribute('type','submit');
                    else
                        b.setAttribute('type','button');

                    b.setAttribute('value',name);

                    if(typeof func === 'function')
                    {
                        if(method !== 'undefined' && method.length>0){
                            if(!close){b.addEventListener(method,function(e){func(e,defer,deferfunc);});}
                            else {b.addEventListener(method,function(e){func(e,defer,deferfunc); $cine().modal.process(close);});}
                        }
                        else{
                            if(!close)b.addEventListener('click',function(e){func(e,defer,deferfunc);});
                            else b.addEventListener('click',function(e){func(e,defer,deferfunc); $cine().modal.process(close);});
                        }
                    } else
                        b.addEventListener('click',function(e){c.modal.close(close);});
                }
                return b;
            }
        };

    this.userkey = function(object){

        if(window.localStorage!=='undefined'){
            object['user']=window.localStorage.getItem('user_id');
            object['userkey']=window.localStorage.getItem('user_key');
        } else {
            object['user']=0;
            object['userkey']="";
        }

    };

    this.userkeyupd = function(object){

        if(window.localStorage!=='undefined'){
            object['user']=window.localStorage.getItem('user_id');
            object['userkey']=window.localStorage.getItem('user_key');
        } else {
            object['user']=0;
            object['userkey']="";
        }
        return object;
    };

    this.creditcard = function(e){
        cineFnUserCardFlow(e);
    };

    /*
    this.modal = {

        windowid:false,

        ident: null,
        special:null,
        requser: null,
        requserkey: false,
        reqtype: null,
        attribs: null,
        query: null,
        btnname: 'Button',
        frmtitle: 'Window',
        apptitle: false,
        centered: false,
        backbar: false,

        submitfunc: false,
        secondfunc: false,
        thirdfunc: false,

        // data = HTML formatted data
        // bfrm = Frame where to set button
        // evnt = Save button event
        // type = Boolean type (desktop format / mobile format)
        // centr = Set the text-align center property
        set: function(data,bfrm,usr,rtyp,attr,type,centr){

            type = typeof type !== 'undefined' ? type : false;

            this.ident = attr.cineident;
            this.centered = centr;
            this.requser = usr;
            this.reqtype = rtyp;
            this.attribs = attr;
            this.query = attr.cinequery;
            this.btnname = attr.cinebtnname;
            this.frmtitle = attr.cinereqtitle;
            this.apptitle = attr.cineapptitle;
            this.special = attr.cinespecial;
            this.requserkey = attr.cineuserkey;
            this.backbar = attr.cinebackbar;

            if(cineObjectIndex(attr,'cinerandom')){
                this.windowid=attr.cinerandom;
            }

            if(type){
                return this.show(type,data,bfrm);
            }

            return false;
        },
        // type = there goes variants of button
        // obj = where to put
        // exec = attributes to place
        // close = object which terminate from DOM
        button: function(type,obj,exec,close){

            var b,p;
            //var c = new cineBox();

            if(type==0){
                b = document.createElement($cineFnMainObjects.domButton);
                b.className = $cineFnMainObjects.btnClass;
            }

            if(type==1){

                p=close.id;
                b = document.createElement($cineFnMainObjects.domButtonForm);
                b.className = 'cine_fn_color_btn';
                b.setAttribute('type','submit');
                b.setAttribute('value',this.btnname);
                b.setAttribute('cineparent',p);
                this.attribs.cinequery=this.attribs.cineback;
                var u=this.attribs;
                for(var k in u){
                    if(u.hasOwnProperty(k))
                        b.setAttribute(k,u[k]);
                }

                if(obj.id.length > 0){
                    b.setAttribute('id','dsadsadsa');
                } else {
                    cn = obj.className;
                    b.setAttribute('id',cn);
                }
                obj.appendChild(b);
                b.addEventListener('click',function(e){$cine().rexec.route(e); $cine().modal.process(close);});
            }

        },
        // pos = type from set prefunction
        // cont = data from set prefunction
        show: function(pos,cont,bfrm){

            var m,i,f,mt,ma,mw,mh,ms,dh;

            dh=window.innerHeight;
            m = document.createElement($cineFnMainObjects.domLayer);
            m.className = $cineFnMainObjects.modalClass+ ' cinemodal view cinevisible slideInLeft';

            if(this.apptitle){
                ma = document.getElementById('cine_fn_ctitle');
                ma.innerHTML=this.apptitle;
                m.setAttribute('cinetitle',this.apptitle);
            }

            f=cont;
            i=this.windowid;

            //m.style.cssText = 'min-width:'+$cineFnMainObjects.modalDefW+'px;min-height:'+$cineFnMainObjects.modalDefH+'px;';
            m.style.width = '100%';
            m.style.zIndex = 9999;
            m.style.position = 'absolute';
            m.setAttribute('cineident',this.ident);
            m.setAttribute('cinespecial',this.special);

            if(!this.windowid){
                i=new Date(); i=i.getDay()+''+i.getHours()+''+i.getMinutes()+''+i.getSeconds()+''+ i.getMilliseconds();
                m.setAttribute('id','cinemodal' + i);
                m.setAttribute('modalid',i);
            } else {
                m.setAttribute('id','cinemodal'+i);
                m.setAttribute('modalid',i);
            }

            if(this.btnname)
                this.button(1,bfrm,'',m);

            f.setAttribute('id','cinecont'+i);

            if(this.backbar){
                var bar;
                if(this.frmtitle.length>0){
                    bar=cineBoxFnRestoMenuBack(false,false,this.backbar,m,this.frmtitle);
                } else
                    bar=cineBoxFnRestoMenuBack(false,false,this.backbar,m);

                bar.style.height='40px';
                m.appendChild(bar);
                this.attribs.cinebackbar=bar.getAttribute('cineident');
            }
            m.appendChild(f);
            //document.body.appendChild(m);
            ms = 'top:76px;';

            if(this.centered)
                ms += 'text-align:center;';

            m.style.cssText = 'min-width:'+$cineFnMainObjects.modalDefW+'px;min-height:'+$cineFnMainObjects.modalDefH+'px;'+ms;
            //m.style.left = '-1000px';
            m.style.top = '76px';
            m.style.height = (XWF.windowHeight-76)+'px';
            //m.style.opacity=0;
            m.style.width = cineViewW+'px';

            cineFnCurrentWindow=m;
            cineFnWindows.push(i);

            return m;
        },

        close: function(e){
            if(typeof e !== 'undefined')
                e.parentNode.removeChild(e);
        },

        process: function(object){
            event.preventDefault();
            this.close(object);
        },

        destroyThis: function(o){
            cineWinCtrlRemWin(false,o);
        },

        destroyCurrent: function(){
            cineWinCtrlRemWin(true);
        },
        setModalLoaded: function(o){
            cineBoxFnModalAfterLoad(o);
        }

    };
    */
    this.action = {

        cardprocess: function(e,func,defer,deferfunc){
            cineBoxFnloadScript($cineFnMainObjects.bTreeLink,e,func,defer,deferfunc);
        },
        cartOpen: function(e){

            if(window.localStorage!=='undefined'){
                item=window.localStorage.getItem('userorder');

                if(item!=null){
                    if(item.length>5)
                    {
                        item=JSON.parse(item);
                        cineBoxFnRestoOrderList(e,item);
                    }
                } else {
                    item=JSON.parse('{"user":0,"order":0,"key":""}');
                    cineBoxFnRestoOrderList(e,item);
                }

            }
        }

    };

    this.helper = {

        domTraverse: function(obj,param){
            return cineBoxFnDomTraverse(obj,param);
        },
        domToJson: function(domArray){
            return cineBoxFnEntityToJSON(domArray);
        },
        domCreateEl: function(t,i,c,d,cd,s,sd){
            return cineCreateDomEl(t,i,c,d,cd,s,sd);
        },
        findClass: function(s,c){
            return cineBoxFnReturnClass(s,c);
        },
        arrayClean: function(a){
            return cineBoxFnArrayFilled(a);
        },
        frameLabeled: function(o){
            return cineBoxLabelFrame(o);
        },
        timeToSelect: function(t,o,i){
            return cineMakeDays(t,o,i);
        },
        styleToPx: function(t){
            return cineBoxFnCoordPxRem(t);
        }

    };

    this.resto = {
        orderBegin: function(e,i,t){
            cineBoxFnRestoOrder(e,i,t);
        },
        getMenu: function(e,d){
            cineBoxFnRestoOrderMenu(e,d);
        },
        renderMenu: function(d,l,g,sg,i){
            cineBoxFnRestoRendrMenu(d,l,g,sg,i);
        }
    };

    this.menu = {
        sideSet: function(e){
            cineBoxFnRestoMenuSideSet(e);
        }
    };

    this.order = {
        coItems: function(o,i,k,f,a){
            return cineBoxFnRestoOrderItemList(o,i,k,f,a);
        },
        coSummary: function(p,t,u,d,dt){
            return cineBoxFnRestoOrderItemSumm(p,t,u,d,dt);
        },
        price: function(type){
            return this.retNumber(cOrderPrice.priceCash,type);
        },
        tax: function(type){
            return this.retNumber(cOrderPrice.taxCash,type);
        },
        discount: function(type){
            return this.retNumber(cOrderPrice.discCash,type);
        },
        tip: function(type){
            return this.retNumber(cOrderPrice.tipCash,type);
        },
        total: function(type){
            return this.retNumber(cOrderPrice.totalCash,type);
        },
        countPrices: function(p,t,u,d,dt){
            cOrderPriceCount(p,t,u,d,dt);
        },
        setNumbers: function(price,tax,tips,total,disc,taxP,tipP,disP,disT){
            cOrderPrice.priceCash=price;
            cOrderPrice.taxCash=tax;
            cOrderPrice.tipCash=tips;
            cOrderPrice.totalCash=total;
            cOrderPrice.tipPercent=tipP;
            cOrderPrice.taxPercent=taxP;
            cOrderPrice.discCash=disc;
            cOrderPrice.discValue=disP;
            cOrderPrice.discType=disT;
        },
        retNumber: function(num,type){
            if(typeof type!=='undefined'){
                if(type=='num'){
                    return num;
                } else if (type=='str'){
                    return num.toFixed(2);
                }
            } else {
                return num;
            }
        }
    };

    this.error = function(o,e,m,r){
        cineBoxFnErrorParse(o,e,m,r);
    };

    this.activeorder = {
        init: function(s,o){
            return cFnRestoOrderActive(s,o);
        },
        start: function(){
            return cFnRestoOrdersActiveList();
        }
    };
};
    /*
    cineBox.prototype.ajax = function(type,url,data,event){
        if(type=='post'){
            var a=this.ajaxfunc.initObj();
            if(a) return this.ajaxfunc.postData(a,url,data,event);
        }
    };
    */

    // AJAX HELPER SERIALIZE TO URL // JS Object notation {"key":value,...} to key=value&....
    function cineBoxFnObjToPost(data,qtype){
        var r = [];
        for (var k in data){
            r.push(encodeURIComponent(k) + '=' + encodeURIComponent(data[k]));
        }r.push('query_type='+qtype); return r.join('&');
    }

    function cineBoxFnErrorParse(data,error,errorMsg,repeatFunc){

        if(error==1000)
        {
            var loginObject;
            var windowContent=data.content;

            if(windowContent!=null)
            {

                loginObject=cineBoxFnRequireLogin(windowContent,repeatFunc);
                windowContent.appendChild(loginObject);


            } else {

                obj=document.getElementsByClassName('cineRoot');
                loginObject=cineBoxFnRequireLogin(obj[0],repeatFunc);
                obj[0].style.textAlign='center';
                obj[0].appendChild(loginObject);

            }
            return false;
        }

        if(error==1100){
            cineFnUserCardAskFor();
            return false;
        }

    }

    function cineBoxFnRequireSignup(e,o,r,form){

        // Open Modal window for TOS

        var cc=o.getAttribute('modalid');
        if(cc!=null)
            cc=document.getElementById('cinecont'+cc);
        else
            cc=o;

        var uemail='cine_fn_email';
        //var uname='cine_fn_username';
        var upass='cine_fn_password';
        var upassr='cine_fn_password2';
        var ufname='cine_fn_firstname';
        var ulname='cine_fn_lastname';
        var uphone='cine_fn_phone';

        var f=document.createElement('form');
        f.className='cine_fn_form_default fadeInDown';
        var h='';

        h+='<label>Email Address</label>';
        h+='<input type="text" name="'+uemail+'" id="'+uemail+'" value="">';
        //h+='<label>Unique Login</label>';
        //h+='<input type="text" name="'+uname+'" id="'+uname+'" value="">';
        h+='<label>Password</label>';
        h+='<input type="password" name="'+upass+'" id="'+upass+'" value="">';
        h+='<label>Confirm Password</label>';
        h+='<input type="password" name="'+upassr+'" id="'+upassr+'" value="">';
        h+='<label>First Name</label>';
        h+='<input type="text" name="'+ufname+'" id="'+ufname+'" value="">';
        h+='<label>Last Name</label>';
        h+='<input type="text" name="'+ulname+'" id="'+ulname+'" value="">';
        h+='<label>Phone Number </label>';
        h+='<input type="text" name="'+uphone+'" id="'+uphone+'" value="">';

        var id=form;

        f.innerHTML=h;
        f.style.marginTop='20px';
        f.style.marginBottom='20px';
        var snb=document.createElement('button');
        snb.className='cine_fn_color_btn';
        snb.style.width=230+'px';
        snb.setAttribute('type','button');
        snb.textContent='Sign up';
        snb.setAttribute('id','cine_login_button');
        snb.addEventListener('click',function(e){cineBoxFnProcessRegister(e,this.func,this.creds,this.form)}
            .bind({func:r,creds:{email:uemail,phone:uphone,pass:upass,pass2:upassr,firstname:ufname,lastname:ulname},form:id}));
        f.appendChild(snb);

        snb=document.createElement('h4');
        snb.innerHTML='Already have an account?';
        //snb.style.width=200+'px';
        f.appendChild(snb);

        snb=document.createElement('button');
        snb.className='cine_fn_color_btn';
        snb.style.width=230+'px';
        snb.setAttribute('type','button');
        snb.innerHTML='Login';
        snb.setAttribute('id','cine_signup_button');
        snb.addEventListener('click',function(){this.obj.scrollTop=0; cineBoxFnRequireLogin(this.obj,this.ret,true)}.bind({obj:o,ret:r}));
        f.appendChild(snb);

        cineFnClearDOM(cc);
        cc.appendChild(f);

        // Wait for TOS upload
        if(!cineTOS){
            cineBoxSignupTOS();
        }

    }

    function cineBoxSignupTOS(){
        XWF.connQueryObject = {query:"appinfo", type:"gettos"};
        XWF.connQueryData = {ident:1,ext:'api'};
        $cine().userkey(XWF.connQueryData);
        XWF.send({
            waitCallback:true,
            onComplete:cineBoxShowTOS
        });
    }

    function cineBoxShowTOS(data){
        var o=data.data;
        if(o.success)
        {
            var tm=cOpenTinyModal({
                buttonShow:true,
                buttonName:'Accept',
                buttonEvent:function(){cineTOS=true;}
            });

            var tos=document.createElement('div');
            tos.className='touch-scroll';
            tos.style.cssText='height:80%; width:100%; border-bottom:1px solid #CCCCCC;';
            tos.innerHTML=o.result['content'];
            tm.content.appendChild(tos);
        }
    }

    function cineBoxFnProcessRegister(e,r,n,f){

        e.preventDefault();

        var cur;
        var dat={};
        for(var k in n){
            if(n.hasOwnProperty(k)) {
                cur = document.getElementById(n[k]);
                if (k == 'cine_fn_phone') {

                }
                dat[k] = cur.value;
            }
        }

        XWF.connQueryObject = {query:"userauth",type:"makeuserregister"};
        XWF.connQueryData = {ident:0,ext:'api',data:JSON.stringify(dat)};
        $cine().userkey(XWF.connQueryData);
        XWF.send({
            waitCallback:true,
            repeatFunction:r,
            onComplete:cineBoxFnAfterLogin
        });

    }
    function cineBoxFnRequireLogin(e,r,t){

        var uname='cine_fn_username';
        var upass='cine_fn_password';

        var f=document.createElement('form');
        f.className='cine_fn_form_default fadeInDown';
        f.style.cssText="position:absolute; width:276px; height:316px; top:50%; left:50%; margin-left:-138px; margin-top:-158px;";
        var h='';

        h+='<label>Email Address</label>';
        h+='<input type="text" name="cine_fn_username" id="'+uname+'" value="">';
        h+='<label>Password</label>';
        h+='<input type="password" name="cine_fn_password" id="'+upass+'" value="">';

        var id=e.getAttribute('id');

        f.innerHTML=h;
        var snb=document.createElement('button');
        snb.className='cine_fn_color_btn';
        snb.style.width=230+'px';
        snb.setAttribute('type','button');
        snb.textContent='Login';
        snb.setAttribute('id','cine_login_button');

        snb.addEventListener('click',function(e){cineBoxFnProcessLogin(e,this.func,this.uname,this.upass,this.form)}.bind({func:r,uname:uname,upass:upass,form:id}));
        f.appendChild(snb);

        snb=document.createElement('h4');
        snb.textContent='Don’t have an account yet?';
        snb.style.width=230+'px';
        f.appendChild(snb);

        snb=document.createElement('button');
        snb.className='cine_fn_color_btn';
        snb.style.width=230+'px';
        snb.setAttribute('type','button');
        snb.textContent='Create an Account';
        snb.setAttribute('id','cine_signup_button');
        snb.addEventListener('click',function(e){cineBoxFnRequireSignup(e,this.obj,this.ret,this.form)}.bind({obj:e,ret:r,form:id}));
        f.appendChild(snb);

        if(typeof t!=='undefined'){
            if(cineBoxFnReturnClass(e.className,'cinemodal')){
                var a=e.getAttribute('modalid');
                e=document.getElementById('cinecont'+a);
            }
            cineFnClearDOM(e);
            e.appendChild(f);
        }
        else
            return f;
    }

    function cineBoxFnProcessLogin(e,r,n,p,f){

        e.preventDefault();

        var lg=document.getElementById(n);
        var pw=document.getElementById(p);

        XWF.connQueryObject = {query:"userauth",type:"makeuserlogin"};
        XWF.connQueryData = {ident:lg.value,ext:'api',data:pw.value,user:0};

        XWF.send({
            waitCallback:true,
            repeatFunction:r,
            onComplete:cineBoxFnAfterLogin
        });

    }

    function cineBoxFnAfterLogin(data){

        var d = data.data;

        if(d.success){

            var res= d.result;
            if(window.localStorage!=='undefined'){
                window.localStorage.setItem('user_id',res['user_id']);
                window.localStorage.setItem('user_key',res['user_key']);
            }

            if(XWF.privateViewCurrent!=null)
            {
                XWF.closewindow();
            }
            else
            {
                var obj=document.getElementsByClassName('cineRoot');
                if(obj.length>0){
                    cineFnClearDOM(obj[0]);
                }
            }

            data.repeat();

        } else {


        }

    }

    // COMPUTED STYLE TO INTEGER
    function cineBoxFnCoordPxRem(t){var r=/[(px+)(%+)]+/g;return parseInt(t.replace(r,''));}
    // OBJECT FAST INDEX SEARCH PROTOTYPES
    Array.prototype.indexElm = function(val){var l=this.length;for(var i=0;i<l;i++){if(this[i]===val)return {result:true,pos:i}}return false;};
    // Object.prototype.indexElm = function(val){for(var i in this){if(i===val)return true}return false;};
    // Rewrite object proto to function in case of jquery
    function cineObjectIndex(obj,val){for(var i in obj){if(i===val)return true}return false;};
    // OBJECT ARRAY CLEAN TO ONLY FILLED CELLS and RETURN ARRAY / LENGTH / STRING COMMA SEPARATED
    function cineBoxFnArrayFilled(a){var l=a.length; var e=l-1; var n=[]; var s=0; var v=''; console.log(a);
        for(var i=0;i<l;i++){
            if(a.hasOwnProperty(i)){
                if(a[i].length>0){
                    n[i]=a[i];
                    s++;
                    if(i<l-1)
                        v+=a[i]+',';
                    else
                        v+=a[i];
                }
            }
        }
        return {array:n,length:s,string:v};
    }
    // RETURN CLASS NAME OF FALSE
    function cineBoxFnReturnClass(s,c){
        var r=false;
        if(typeof(s)=='string'){s=s.split(' ');
            for(var i=0;i<s.length;i++){if(s[i]==c){r=s[i];}}
        }return r;}
    // DOM ELEMENTS (which can have value) TO JSON STRING
    function cineBoxFnEntityToJSON(obj){
        var i,l,z,r; r=false;
        if(Object.prototype.toString.call(obj) == '[object Array]'){l=obj.length;z=l-1;r='';
            for(i=0;i<l;i++){
                if(i!=z)
                    r+='"'+obj[i].id+'":"'+obj[i].value+'",';
                else
                    r+='"'+obj[i].id+'":"'+obj[i].value+'"';
            }
            r='{'+r+'}';
        }
        return r;
    }

    // FIND MODAL PARENT
    function cineBoxFnFindModal(obj){
        var tobj,cls='cinemodal';
        if(typeof obj === 'string'){
            obj = document.getElementById(obj);
        }
        if(typeof obj === 'object'){
            tobj=obj;
            do {
                if(cineBoxFnReturnClass(tobj.className,cls)){
                    return tobj;
                } else {
                    tobj=tobj.parentNode;
                }
            } while(tobj.tagName !== 'BODY');
        }
        return false;
    }

    // LOAD EXTERNAL LIBRARY
    function cineBoxFnloadScript(url,obj,callback,defer,deferfunc){
        var s = document.createElement("script");
        s.type = "text/javascript";
        if (s.readyState){
            s.onreadystatechange = function(){
                if (s.readyState == "loaded" || s.readyState == "complete"){
                    s.onreadystatechange = null;
                    callback(obj,defer,deferfunc);
                }
            };
        } else {
            s.onload = function(){callback(obj,defer,deferfunc);};
        }
        s.src = url;
        document.getElementsByTagName("head")[0].appendChild(s);
    }
    // DOM TREE TRAVERSE
    function cineBoxFnDomTraverse(obj,objclass){

        var cur,ti,curc,up,uppn;
        var t=['NONE','INPUT','SELECT','TEXTAREA'];
        var cl=false;

        if(objclass != null || typeof objclass !== 'undefined'){
            objclass = objclass.split(':');
            if(objclass[0]!=='*'){
                t=[objclass[0].toUpperCase()];}
            cl=objclass[1];
        }

        var d=[];

        if(obj!=null){

            var s=["none",obj.id];
            var c=obj.childNodes;
            var k=c.length;
            var z=0;

            for(var i=0;i<k;i++){
                cur = c[i];

                if(cur.nodeType != 3){

                    if(cur.id == null || cur.id == ''){
                        ti = new Date();
                        ti=ti.getHours()+''+ti.getMinutes()+''+ti.getSeconds()+''+ti.getMilliseconds()+''+Math.round((999)*Math.random());
                        cur.setAttribute('id','cine-fdt' + ti);
                    }

                    if(!s.indexElm(cur.id)){

                        if(!t.indexElm(cur.tagName)){

                            curc = cur.childNodes;
                            if(curc.length == 1 && curc[0].nodeType != 3){
                                c=curc; i=-1; k=c.length;
                            }
                            if(curc.length > 1){
                                c=curc; i=-1; k=c.length;
                            }
                        }
                        else {

                            if(cl){ // CLASS SPECIFIED
                                if(cineBoxFnReturnClass(cur.className,cl)){
                                    d.push(cur);}
                            }
                            else{ // NOT CLASS SPECIFIED
                                d.push(cur);}
                        }
                    }
                    s.push(cur.id);
                }
                if(i==(k-1)){
                    uppn=cur.parentNode;
                    if(uppn != obj){
                        up = uppn.parentNode;
                        c=up.childNodes;
                        i=-1;
                        k=c.length;
                    }
                }
                z++; if(z>1000) break;
            }

            return d;
        }
    }

    /* NAVIGATION -> GOOGLE
    function cineBoxParseGeoQuery(o){
        if(o.status=="OK"){
            var t={sublocality_level_1:'long_name',locality:'long_name',administrative_area_level_1:'long_name',route:'long_name',postal_code:'long_name'};
            var r={sublocality_level_1:false,locality:false,administrative_area_level_1:false,route:false,postal_code:false};
            var l={street_address:false,neighborhood:false};
            var s=o.results;
            var c,e,a;

            for(var k in s){

                if(s.hasOwnProperty(k)){
                    c=s[k].types[0];
                    if(cineObjectIndex(l,c))l[c]=s[k].address_components;
                }
            }

            for(k in l){
                if(l.hasOwnProperty(k)){
                    c=l[k];
                    for(var d in c){
                        e=c[d];
                        if(e.hasOwnProperty('types')){
                            a=e.types[0];
                            if(cineObjectIndex(r,a)){r[a]=e[t[a]];}
                        }
                    }
                }
            }
            return r;
        }
        return false;
    }
    */
    /* NAVIGATION TEXT -> HUMAN STRING (Searchable navigation)
    function cineBoxObjToCommaString(obj){
        var res='';
        for(var k in obj){
            if(obj.hasOwnProperty(k) && obj[k]){
                res+=obj[k]+', ';
            }
        }
        res=res.split('');
        //var l=(res.length-1);
        res.pop();
        res.pop();
        res=res.join('');
        //console.log(res);
        return res;
    }
    */
    function cineBoxFnObjRestoList(data)
    {

        //var list=document.getElementById('cine_fn_resto_list');
        //cineFnClearDOM(list);
        var obj=data.data;
        //if(list!=null && obj.length>0){

        if(obj.success)
        {
            //obj=JSON.parse(obj);
            var m=0.621;
            //var y=0.915;
            var f=5280;
            var v=0;
            var vm=0;
            var t;
            var r=obj.result;
            var g=obj.geo;
            var d=0;
            var n=[];
            //var adr,ret,inr,btn;
            //var htm='';

            var pos=data.object;
            var lat=pos.coords.latitude;
            var lng=pos.coords.longitude;

            for(var k in g){
                if(g.hasOwnProperty(k)){
                    d=cineBoxFnRealDist(lat,lng,g[k].lat,g[k].lng);
                    n.push({d:d,i:k});
                }
            }

            var nocord=false;
            if(lat==0)
                nocord=true;

            n.sort(function(a,b){
                if (a.d > b.d){return 1;}
                if(a.d < b.d){return -1;}
                return 0;
            });

            //var lt=cineCreateDomEl('table','','cartTable table-responsive','');
            cineFnRestoList={};
            cineFnRestoType=obj['type'];
            for(var i in n){

                //ret=null;

                if(n.hasOwnProperty(i)){

                    var cur=n[i].i;

                    if(!nocord){
                        v=n[i].d;
                        vm=(v*m).toFixed(2);
                        t='mi';
                        //if(vm<0.5){
                        //    vm=((v*1000)/y).toFixed(0);
                        //    t='yards';
                        //}
                        if(vm<0.185){
                            vm=(vm*f).toFixed(0);
                            t='ft';
                        }
                    } else {
                        vm='';
                        t='';
                    }

                    rcur=r[cur];
                    cineFnRestoList[i]={id:Number(cur),name:rcur['name'],desc:rcur['description'],type:rcur['type'],addr:rcur['address'],dist:vm+' '+t};

                }

            }

        }
        return false;
    }

    function cineBoxFnRestoOrder(e,r,t){

        if(typeof t==='undefined'){
            t='Restaurant Menu';
        }

        cineFnRestoTitle=t;

        XWF.connQueryObject = {query:"restoinfo", type:"getrestomenu"};
        XWF.connQueryData = {ident:r,user:0};

        XWF.send({
            navigationBar:true,
            waitCallback:true,
            raiseWindow:true,
            raiseWindowID:"default",
            windowClass:'theme',
            onComplete:cineBoxFnRestoOrderMenu,
            changeTitle:t,
            windowTitle:"Restaurant Menu"
        });
    }

    function cineBoxFnIsRestoMenuOnScreen(){
        var elem = document.getElementById('cine_resto_menu_anchor');
        return (elem!=null);
    }

    function cineBoxFnRestoOrderMenu(data){

        var proc=false;

        var cid=data.content;
        var d=data.data;

        if(d.success){
            proc=true;

            if(!d.result)
                cineFnMenuArray={};
            else
                cineFnMenuArray=d.result;

            if(!d.side)
                cineFnSidesArray={};
            else
                cineFnSidesArray=d.side;

            var mnu=cineBoxFnRestoRendrMenu(d.result,1,null,false,d.resto);
            var mnuAnchor=document.createElement('input');
            mnuAnchor.setAttribute('type','hidden');
            mnuAnchor.setAttribute('id','cine_resto_menu_anchor');
            cid.appendChild(mnuAnchor);
            cid.appendChild(mnu);
        }

        if(!proc){
            mnu=document.createElement('div');
            mnu.className = 'cine_fn_modal_centerer';
            mnu.innerHTML = 'Error in server query';
            cid.appendChild(mnu);
        }
    }

    function cineBoxFnRestoRendrMenu(d,l,g,sg,i){


        var e,mf,n,k,b,cur,cur2,prev,c,cntr,obj;

        if(!d){
            d=cineFnMenuArray;
            if(!d || Object.keys(d).length<1){
                e=document.createElement('div');
                n=document.createElement('div');
                n.className='cine_list_item cine-list-small';
                n.innerHTML='<label> That section have no items </label>';
                e.appendChild(n);
            }
        }

        if(typeof d==='object'){

            if(l==1){
                e=document.createElement('div');
                //e.className = 'cine_fn_modal_centerer';
                for(k in d){
                    if(d.hasOwnProperty(k)){
                        n=document.createElement('div');
                        n.className='cine_list_item cine-list-small';
                        n.innerHTML=''+d[k].title+'';
                        n.addEventListener('click',function(e){cineBoxFnRestoRendrMenu(false,this.l,this.g,false,this.r)}.bind({g:k,l:l+1,r:i}));
                        e.appendChild(n);
                    }
                }

            }else if(l==2){

                cur = d[g].include;

                //c = new cineBox();
                cntr = document.createElement('div');
                //cntr.className = 'cine_fn_modal_centerer';
                //cntr.style.width='100%';

                if(cur) {

                    for (k in cur) {
                        if (cur.hasOwnProperty(k)) {
                            n = document.createElement('div');
                            n.className='cine_list_item cine-list-small';
                            n.innerHTML = '' + cur[k].title + '';
                            n.addEventListener('click',function(e){
                                cineBoxFnRestoRendrMenu(false, this.l, this.g, this.sg, this.r)
                            }.bind({g: g, sg: k, l: l + 1, r: i}));
                            cntr.appendChild(n);
                        }
                    }
                } else {
                    n=document.createElement('div');
                    n.className='cine_list_item cine-list-small';
                    n.innerHTML='That section have no items';
                    cntr.appendChild(n);
                }

                win=XWF.openwindow({
                    navigationBar:true,
                    raiseWindow:true,
                    windowClass:'theme',
                    changeTitle:cineFnRestoTitle,
                    windowSubTitle:"Restaurant Menu"
                });

                win.append(cntr);

            }else if(l==3){

                cur = d[g].include;
                cur2= cur[sg].include;

                //c = new cineBox();
                cntr = document.createElement('div');
                //cntr.className = 'cine_fn_modal_centerer';
                //cntr.style.width='100%';

                win=XWF.openwindow({
                    navigationBar:true,
                    raiseWindow:true,
                    windowClass:'theme',
                    changeTitle:cineFnRestoTitle,
                    windowSubTitle:"Restaurant Menu"
                });

                //win.append(cntr);

                if(cur2){

                    for (k in cur2) {
                        if (cur2.hasOwnProperty(k))
                        {
                            n = document.createElement('div');
                            n.className = 'cine_list_item cine-list-small';
                            n.innerHTML = '<div class="cine_fn_resto_menu_eit">' + cur2[k].title +
                            '<br/><span style="padding-top:5px; font-size:12px;">' + cur2[k].desc +
                            '</span></div><div class="cine_fn_resto_menu_eip"><span style="float: right; padding-right: 5px;"> $' + cur2[k].price + '</span></div>';
                            //n.innerHTML = cur2[k].title +
                            //'' + cur2[k].desc +
                            //'$' + cur2[k].price;
                            //    '';

                            n.addEventListener('click', function (e) {
                                cineBoxFnRestoItemOrder(this.i, this.d, this.g, this.r)
                            }.bind({i: k, d: cur2[k], g: e, r: i}));
                            cntr.appendChild(n);
                            //win.append(n);
                        }
                    }
                } else {
                    n=document.createElement('div');
                    n.className='cine_list_item cine-list-small';
                    n.innerHTML='That section have no items';
                    cntr.appendChild(n);
                    //win.append(n);
                }

                win.append(cntr);

            }
        }
        return e;
    }

    function cineBoxFnRestoItemOrder(id,d,p,r){

        cineSideOptions={};

        var e,b,n,sn, k, c,cntr,vars,obj;

        //c = new cineBox();
        cntr = document.createElement($cineFnMainObjects.domLayer);
        cntr.className = 'cine_fn_modal_centerer';
        cntr.style.width='100%';

        n=document.createElement('div');
        n.className='cine_fn_resto_menu_i_d';

        cinePriceObject={i:id,p:Number(d.price),n:d.title,d:d.desc,q:1,s:false};

        sn=document.createElement('div');
        sn.className='cine_fn_resto_menu_i_t';
        sn.innerHTML='<h4 style="padding-left:0; !important;">'+d.title+'</h4>';
        n.appendChild(sn);

        sn=document.createElement('div');
        sn.className='cine_fn_resto_menu_i_i';
        sn.innerHTML=''+d.desc+'';

        n.appendChild(sn);
        cntr.appendChild(n);

        n=document.createElement('div');
        n.className='cine_fn_resto_menu_i_p';
        sn=document.createElement('div');
        sn.className='cine_fn_resto_menu_i_op';
        sn.setAttribute('id','cine_fn_mlip'+id);
        sn.style.cssText="text-align:right !important;";
        sn.innerHTML='<i class="fa fa-usd" style="font-size:24px;"></i> '+d.price+'';
        n.appendChild(sn);

        cinePriceTitle=sn;

        //sn=document.createElement('div');
        //sn.className='cine_fn_resto_menu_i_ops';
        //n.appendChild(sn);

        sn=document.createElement('div');
        sn.className='cine_fn_resto_menu_i_o';
        sn.setAttribute('id','cine_fn_mlio'+id);
        snb=document.createElement('button');
        snb.className='cine_fn_color_btn';
        snb.setAttribute('type','button');
        snb.innerHTML="<div class='cart_fn_cartglyph inline'></div> Add";
        snb.setAttribute('id','cine_item'+id);
        snb.setAttribute('cineident',id);
        snb.style.cssText='padding-top:10px !important; margin-right:0 !important;';
        snb.addEventListener('click',function(e){cineBoxFnRestoItemToOrder(e,this.id,this.r);}.bind({id:id,r:r}));
        sn.appendChild(snb);

        n.appendChild(sn);
        cntr.appendChild(n);

        var s;
        s=d.include;
        if(typeof s==='string'){
            s=s.split(',');
        }

        var ds;
        ds=cineFnSidesArray;
        if(!ds || Object.keys(ds).length<1){
            ds={};
        }

        var ea=[];

        for(k in s){
            if(s.hasOwnProperty(k)){

                if(cineObjectIndex(ds,s[k])){

                    var cur=ds[s[k]];
                    var sv=cineBoxFnRestoSideRender(cur,s[k],id);

                    if(sv.type){
                        n=cineBoxLabelFrame({title:cur.name,elems:[sv.elem]});
                        cntr.appendChild(n);
                    }
                    else
                        ea.push(sv.elem);

                }
            }
        }

        if(ea.length>0) {
            n = cineBoxLabelFrame({title: 'Add your favorites', elems: ea});
            cntr.appendChild(n);
        }

        sn=document.createElement('textarea');
        sn.setAttribute('id','cine_fn_mlit'+id);
        sn.style.cssText = 'width:100%; height:80px; resize:none';
        n=cineBoxLabelFrame({title:'Special Instructions',elems:[sn],color:'transparent',notpad:true});

        cntr.appendChild(n);

        win=XWF.openwindow({
            navigationBar:true,
            raiseWindow:true,
            windowClass:'theme',
            changeTitle:cineFnRestoTitle
        });

        win.append(cntr);

    }

    function cineBoxFnRestoSideRender(d,s,id){

        var basePrice=Number(d['price']);

        var out={type:false,elem:false};

        var ret,tr1,td;
        var k,l=0,mode=false;
        var dv = d.variant;
        var dk = Object.keys(dv);
        var tdp=[];
        var tdb=[];
        var b;

        if(dk.length>0){

            for(k in dv){
                if(dv.hasOwnProperty(k)){

                    varPrice=Number(dv[k]);
                    b=document.createElement('button');
                    b.className='cine_fn_color_btn_i cine_side_btn';
                    b.setAttribute('value',k);
                    b.setAttribute('number',l);
                    b.setAttribute('scope',s);
                    b.setAttribute('price',varPrice);
                    b.setAttribute('baseprice', basePrice);
                    b.setAttribute('basename', d.name);
                    b.setAttribute('ident',id);
                    b.setAttribute('id',s+l);
                    b.innerHTML=k;

                    if(varPrice>0){
                        mode=true;
                        b.setAttribute('price',varPrice);
                        tdp.push('<div>$ '+varPrice+'</div>');
                    }
                    else if(varPrice==0){
                        b.setAttribute('price','0');
                        tdp.push('<div> Free </div>');
                    } else {
                        b.setAttribute('price','0');
                        tdp.push('<div></div>');
                    }
                    b.addEventListener('click',function(e){cineBoxFnRestoMenuSideSet(e.target)});
                    tdb.push(b);
                    sideb=s+l;
                    cineSideOptions[sideb]=b;
                    l++;

                }
            }

            ret=document.createElement('table');
            ret.style.width = '100%';
            var pts=(l>2)?2:l;

            tr1=document.createElement('tr');
            td=document.createElement('td');
            td.setAttribute('colspan',pts);
            td.className = 'info';
            td.innerHTML=d.desc;
            tr1.appendChild(td);
            ret.appendChild(tr1);

            // If all parts of side could have a price
            if(basePrice>0){

                tr1=document.createElement('tr');
                td=document.createElement('td');
                td.setAttribute('colspan',pts);
                td.innerHTML='$ '+basePrice;
                tr1.appendChild(td);
                ret.appendChild(tr1);

            }
            // If only variants has price
            else if(basePrice<=0) {

                tr1=document.createElement('tr');
                td=document.createElement('td');
                td.setAttribute('colspan',pts);
                td.innerHTML='Free';
                tr1.appendChild(td);
                ret.appendChild(tr1);

            }
            // If any other variant (no price at variants and price at basic, or all equal to zero)

            var len=1;
            var elemRow=null;
            var priceRow=null;
            var rowNotEmpty=false;
            var elemWidth=Math.floor(100/pts);

            for(k=0;k<l;k++){
                if(len>pts){
                    len=1;
                    if(elemRow!=null) {
                        ret.appendChild(elemRow);
                        ret.appendChild(priceRow);
                    }
                    rowNotEmpty=false;
                }
                if(len==1){
                    elemRow=document.createElement('tr');
                    priceRow=document.createElement('tr');
                    rowNotEmpty=true;
                }
                td=document.createElement('td');
                td.style.width=elemWidth+'%';
                td.appendChild(tdb[k]);
                elemRow.appendChild(td);
                td=document.createElement('td');
                td.style.width=elemWidth+'%';
                td.innerHTML=tdp[k];
                priceRow.appendChild(td);
                len++;
            }

            if(rowNotEmpty)
            {
                ret.appendChild(elemRow);
                ret.appendChild(priceRow);
            }

            out.type=true;
            out.elem=ret;

        }
        // If no variants at all (only button to add)
        else {

            b=document.createElement('button');
            b.className='cine_fn_color_btn_i cine_side_btn';
            b.setAttribute('number',1);
            b.setAttribute('scope',s);
            b.setAttribute('baseprice', basePrice);
            b.setAttribute('basename', d.name);
            b.setAttribute('ident',id);
            b.setAttribute('id',s+'1');
            b.innerHTML=d.name;
            b.addEventListener('click',function(e){cineBoxFnRestoMenuSideSet(e.target)});
            sideb=s+'1';
            cineSideOptions[sideb]=b;
            ret=document.createElement('table');
            ret.style.width = '100%';

            tr1=document.createElement('tr');
            td=document.createElement('td');
            td.setAttribute('colspan','3');
            td.className = 'info';
            td.innerHTML=d.desc;
            tr1.appendChild(td);
            ret.appendChild(tr1);

            tr1=document.createElement('tr');
            td=document.createElement('td');
            td.style.width='50%';
            td.appendChild(b);
            tr1.appendChild(td);

            if(basePrice>0)
                price='$ '+ basePrice;
            else
                price= 'Free';

            td=document.createElement('td');
            td.innerHTML = price;
            td.style.width='50%';
            tr1.appendChild(td);

            ret.appendChild(tr1);

            out.elem=ret;

        }
        return out;
    }
    function cineBoxFnRestoMenuSideSet(e){

        var t=0;
        var pn=null;
        var pt=true;

        var s= e.getAttribute('scope');
        var p= e.getAttribute('price');
        var bp=Number(e.getAttribute('baseprice'));
        var i= e.getAttribute('ident');
        var n= e.getAttribute('number');
        var bn=e.getAttribute('basename');

        var ip=cinePriceTitle;
        var d=cinePriceObject;
        //{i:id,p:d.price,n:d.title,d:d.desc,q:1,s:false}
        if(d){
            var ndata={n:bn,v:e.value,i:n,p:bp,p2:p,q:0};

            if(d.s){

                var cur = d.s;
                var fnd=false;

                var x=0;
                for(var k in cur){
                    if(cur.hasOwnProperty(k)){

                        x++;
                        if(k==s){

                            fnd=true;
                            pn=cur[k].i;
                            if(pn!=n)
                                cur[k]=ndata;
                            else {
                                delete cur[k];
                                pt=false;
                                x--;
                            }

                        }
                    }
                }

                if(!fnd){
                    cur[s] = ndata;
                    x++;
                }

                if(x<1)
                    d.s=false;

                t=cineBoxFnRestoIPriceCnt(d);
                ip.innerHTML='<i class="fa fa-usd" style="font-size:24px;"></i> '+t+'';
                cinePriceObject=d;

            } else {

                var elm = {};
                elm[s] = ndata;
                d.s = elm;

                t=cineBoxFnRestoIPriceCnt(d);
                ip.innerHTML='<i class="fa fa-usd" style="font-size:24px;"></i> '+t+'';
                cinePriceObject=d;

            }

            if(pn!=null){
                cineSideOptions[s+pn].className = 'cine_fn_color_btn_i cine_side_btn';
            }
            if(pt)
                e.className += ' cine_btn_active';

        }
    }

    // **************************************************
    // --------------------------------------------------
    // PUT ITEMS FROM MENU TO ORDER AT SERVER AND IN THE
    // CART AS WELL AS IT PRESENTED ONSCREEN
    // --------------------------------------------------
    // **************************************************

    function cineBoxFnRestoItemToOrder(e,id,r){

        //var c=new cineBox();
        var t=cineBoxFnDomObject(e.target);
        var data=JSON.stringify(cinePriceObject);
        var spec=document.getElementById('cine_fn_mlit'+id);
        var cart=document.getElementById('cine_fn_base_cart_items');

        if(spec==null){
            speci='';
        } else {
            speci=cineJsonTextCheck(spec.value);
            if(speci.length<1){
                speci='';
            }
        }

        if(cineFnTimeObject!=null)
        {
            var datetime = cineFnTimeObject.year + '-' + cineFnTimeObject.month + '-' + cineFnTimeObject.day + ' ' + cineFnTimeObject.hour + ':' + cineFnTimeObject.minute + ':00';

            XWF.connQueryObject = {query:"restoorder", type:"putitemtoorder"};
            XWF.connQueryData = {
                ident:r,user:0,data:data,
                adata:'{"datetime":"' + datetime + '","party":"' + cineFnTimeObject.party + '"}',
                ext:cart.value,
                special:speci
            };

            XWF.send({
                navigationBar:true,
                waitCallback:true,
                raiseWindow:false,
                onComplete:cineBoxFnRestoOrderPut
            });

        }
    }

    var cFnRestoCart=null;

    // +++++++++++++++++++++++++++++++++++++++++++++++++

    function cineBoxFnRestoOrderPut(data){

        var obj=data.data;

        if(obj.success)
        {
            cFnRestoCart=obj.result;

            // REWRITE REWRITE REWRITE REWRITE REWRITE REWRITE
            // --- ---  NEED TO GET RID OR REWRITE THIS --- ---
            // Suspicious code which need to be more clear
            if(typeof elem==='undefined' || !elem){
                cineBoxFnCartState(true);
            } else {
                elem.className = 'cine_fn_cart cine_fn_cart_active';
                elem.addEventListener('click',function(e){
                    if(cFnRestoCart!=null && cFnRestoCart){
                        cineBoxFnRestoOrderList(e,cFnRestoCart);
                    }
                });
            }
            // REWRITE REWRITE REWRITE REWRITE REWRITE REWRITE
            // --- ---  NEED TO GET RID OR REWRITE THIS --- ---
            // Old function to populate cart items by JSON string object
            var fc=document.getElementById('cine_fn_base_cart_items');
            fc.value = JSON.stringify(cFnRestoCart);

            // IF CART WINDOW IS ONSCREEN (or opened) SCENARIO THEN
            var cartWindowTable=document.getElementById('c_order_i_'+cFnRestoCart.order);
            if(cartWindowTable !=null){
                // Clear this node from content
                parentFrame=cartWindowTable.parentNode;
                cineFnClearDOM(parentFrame);

                // Render new bunch of items with current restaurant name for order
                ec=cineBoxFnRestoOrderItemList(obj.order,cFnRestoCart.order,cFnRestoCart.key,true,{restaurant:obj.restaurant,restaurant_name:obj.restaurant_name,showAddButton:true});

                // Put the items to the frame
                if(ec!=null)
                    parentFrame.appendChild(ec);

            }

            if(window.localStorage!=='undefined'){
                window.localStorage.setItem('userorder',JSON.stringify(cFnRestoCart));
            }

        }

        // Cancel 2 windows
        XWF.closewindow();
        XWF.closewindow();

    }

    function cineBoxFnCartState(state){

        var cart=document.getElementsByClassName('cine_fn_cart');
        var cn='';

        if(state){
            cn='cine_fn_cart cine_fn_cart_active';
            $cmenu.enable('shopcart');
        } else {
            cn='cine_fn_cart';
            var oCart=document.getElementById('cine_fn_base_cart_items');
            oCart.value='';
            $cmenu.disable('shopcart');
        }

        if(cart.length>0){
            var cur = cart[0];
            var proc=true;
            if(cineBoxFnReturnClass(cur.className,'cine_fn_cart_active')){
                proc=false;
            }
            cur.className = cn;
            if(state && proc)
                cur.addEventListener('click',function(e){
                    if(cFnRestoCart!=null && cFnRestoCart){
                        cineBoxFnRestoOrderList(e,cFnRestoCart);
                    }
                });
        }

    }

    // **************************************************
    // --------------------------------------------------
    // ACTIVE ORDERS IN THE MAIN USER WINDOW
    // --------------------------------------------------
    // **************************************************

    var cineFnOrderKeys=[];

    function cFnRestoOrdersActiveList(){

        XWF.connQueryObject = {query:"restoorder", type:"getorderident"};
        XWF.connQueryData = {
            ident:0,
            data:'',
            ext:1
        };

        $cine().userkey(XWF.connQueryData);

        XWF.send({
            waitCallback:true,
            onComplete:cFnRestoOrderActive
        });
    }

    function cFnRestoOrderActive(data){

        var a,w,i,l,h,o;

        if(data.hasOwnProperty('mode')){
            o=data;
        } else if(data.hasOwnProperty('data')) {
            o=data.data;
        } else {
            o=data;
        }

        cFscrOrd.style.display='inline-block';
        //w=Math.round(cFscrOrdW/100*90);
        w=310;

        // Mode — method which existed in callback for: Active Orders Data
        if(o.hasOwnProperty('mode'))
        {
            if(o.active){
                a=o.active;
            }

            loading=document.getElementById('c_fscr_loading');
            if(loading!=null){
                loading.style.display='none';
            }
        // If Mode not defined
        } else if(o.hasOwnProperty('active')) {
            a=o.active;
        } else {
            a=o;
        }

        h=400;
        if(cineCurrentView=='index'){
            h=200
        }

        if(a){
            cFscrOrdBox=document.createElement('div');
            cFscrOrdBox.className='c_ordbox';
            cFscrOrdBox.style.height=h+'px';
            cFscrOrdBox.style.padding='10px';
            cFscrOrdBox.style.textAlign='left';
            i=Object.keys(a);
            l=i.length;

            cFscrOrdBox.style.width=((w-20)*l)+20+'px';
            cFscrOrdBox.style.left='-5px';
            cFscrOrdBox.style.position='relative';

            cFnRestoOrderActiveAdd(a,false);

            cFscrOrd.appendChild(cFscrOrdBox);
            cFscrOrd.style.height=h+'px';
            cFscrOrd.addEventListener('touchstart',function(e){cTouchStart(e)});
            cFscrOrd.addEventListener('touchmove',function(e){cTouchMove(e,this,cFnRestoOrderActiveList)}.bind(cFscrOrdBox));
            cFscrOrd.addEventListener("touchend",function(e){e.preventDefault();cFnRestoOrderActiveAlign();tCursorDy=0;tCursorH=0;tCursorL=0;tCursorW=0;tCursorM=0}.bind(cFscrOrdBox),false);

            cineFnOrderList=a;
            cineFnOrderKeys=i;
        }
        else
        {

            loading=document.getElementById('c_fscr_loading');
            style=loading.style.cssText;
            if(loading!=null){
                loading.style.display='none';
            }
            notify=document.createElement('div');
            notify.setAttribute('id','c_fscr_noorders');
            notify.style.cssText=style;
            notify.style.color='rgba(0, 195, 167,0.9)';
            notify.style.display='block';
            notify.textContent='You have no pending orders';
            loading.parentNode.appendChild(notify);

        }

    }

    function cFnRestoOrderActiveAdd(o,length){

        var mt=['','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

        var t,k,cur,e,obtn,textDate,guests;
        //w=Math.round(cFscrOrdW/100*90);
        w=310;
        if(w>288){
            w=288;
        }
        nw=w-20;

        for(k in o)
        {
            cur=o[k];

            // ---------------------------------------
            // DATE/TIME count for order
            // ---------------------------------------
                date=cur.d;
            // --> Convert ORDER date to readable OBJECT
                ti = cTimeObject(date);

            // --> Determine current DAY and next DAY
                date=new Date();
                cday=date.getDate();
                ndate=date.getTime()+86400000;
                ndate=new Date(ndate);
                nday=ndate.getDate();

            // --> Compare DATE values
                if(ti.d==cday){
                    textDate='Today at '+cineTimeNormalize(ti.hour,ti.minute);
                } else if (ti.d==nday){
                    textDate='Tomorrow at '+cineTimeNormalize(ti.hour,ti.minute);
                } else {
                    textDate=ti.day+' '+mt[ti.m]+' at '+cineTimeNormalize(ti.hour,ti.minute);
                }
            // ---------------------------------------
            // ---------------------------------------

            // Create container
            e=document.createElement('div');
            e.setAttribute('id','c_ordelem'+k);
            e.className='c_ordelem';
            e.style.width=(nw)+'px';

            // Output title
            stt=Number(cur['s']);
            ic=document.createElement('h2');
            ic.style.minWidth=0;
            ic.textContent=(stt<6)?'You have a reservation at '+cur['n']:'Your meal at '+cur['n']+' is complete';
            e.appendChild(ic);
            // Output second string
            // --------------------
            // --> If View set to the INDEX page
            if(cineCurrentView=='index'){
                t=document.createElement('table');
                t.className='full-width';
                // --> Output time
                tr=document.createElement('tr');
                td=document.createElement('td');
                td.className='cine_fscr_rtdl';
                td.style.padding='10px 0 10px 0';
                td.textContent=textDate;
                tr.appendChild(td);
                t.appendChild(tr);
                e.appendChild(t);
            }
            // --> If View set to the ORDERS page
            else
            {

                t=document.createElement('table');
                t.className='full-width';

                tr=document.createElement('tr');
                td=document.createElement('td');
                td.className='half-width';
                ic=document.createElement('h1');
                ic.textContent='Order';
                ic.style.marginTop='20px';
                td.appendChild(ic);
                tr.appendChild(td);

                td=document.createElement('td');
                td.className='half-width';
                ic=document.createElement('h1');
                ic.textContent='\u2116'+k;
                ic.style.marginTop='20px';
                td.appendChild(ic);
                tr.appendChild(td);

                t.appendChild(tr);

                // Guests
                g=Number(cur.v['size']);
                if(g<2){
                    guests='1 guest';
                } else {
                    guests=g+' guests';
                }

                // Output time
                tr=document.createElement('tr');
                td=document.createElement('td');
                td.className='cine_fscr_rtdl';
                td.style.cssText='font-size:22px !important; padding-left:20px !important; text-align:left !important;';
                td.setAttribute('colspan',2);
                td.textContent=textDate;
                tr.appendChild(td);
                t.appendChild(tr);
                // Output guest
                tr=document.createElement('tr');
                td=document.createElement('td');
                td.className='cine_fscr_rtdl';
                td.style.cssText='font-size:22px !important; padding-left:20px !important; text-align:left !important;';
                td.setAttribute('colspan',2);
                td.textContent=guests;
                tr.appendChild(td);
                t.appendChild(tr);
                // SPACER
                tr=document.createElement('tr');
                td=document.createElement('td');
                td.setAttribute('colspan',2);
                td.style.height='32px';
                td.textContent='';
                tr.appendChild(td);
                t.appendChild(tr);

                e.appendChild(t);

            }

            obtn=document.createElement('button');
            obtn.className='cine_fn_black_btn';

            obtn.textContent=(stt<6)?'Check Status':'Pay my Bill';
            obtn.addEventListener('touchstart',function(e){cFnRestoOrderActiveGet(e,this)}.bind({id:k,key:cur['k'],sta:stt}));
            obtn.addEventListener('click',function(e){cFnRestoOrderActiveGet(e,this)}.bind({id:k,key:cur['k'],sta:stt}));

            e.appendChild(obtn);
            cFscrOrdBox.appendChild(e);

        }

        if(typeof length!=='undefined'){

            if(length){
                cFscrOrdBox.style.left=-5+'px';
                cw=cFscrOrdBox.style.width;
                cw=cineBoxFnCoordPxRem(cw);
                cFscrOrdBox.style.width=(nw+cw)+'px';
                cFscrOrd.style.display='inline-block';
            }

        }

    }

    function cFnRestoOrderActiveGet(e,o){

        XWF.connQueryObject = {query:"restoorder", type:"getuserorderdata"};
        XWF.connQueryData = {
            ident:o.id,
            data:o.key,
            ext: o.sta,
            userkey:""
        };

        $cine().userkey(XWF.connQueryData);

        XWF.send({
            navigationBar:true,
            waitCallback:true,
            raiseWindow:true,
            raiseWindowID:"cart",
            windowClass:'theme',
            onComplete:cFnRestoOrderActiveShow,
            changeTitle:"Dine-out Status",
            windowTitle:""
        });

    }

    function cFnRestoOrderActiveShow(data){

        var c=data.content;
        var obj=data.data;

        s= ['Your order has not sent to restaurant yet',"Your dine-out request <br /> has been sent",
            'Restaurant now reviewing <br /> your order','Restaurant has rejected your request',
            'Your dine-out request <br />has been accepted'];
        ns= ['','Hold tight. We\'ll be in touch soon','Hold tight. We\'ll be in touch soon','','Check in with the host upon arrival at the restaurant',''];

        if(obj.success) {

            var r = obj.result;
            r['dst']=obj.dst;
            var partner = Number(r.resto_partner);

            var content=document.createElement('div');

            if (partner>0){
                content=cFnAOPartnerRestaurant(r);
            } else {
                content=cFnAOCommonRestaurant(r);
            }

            c.appendChild(content);
        }
    }

    function cFnAOCommonRestaurant(data){

        var s= ['Your order has not sent to restaurant yet',"Your dine-out request <br /> has been sent",
            'Restaurant now reviewing <br /> your order','Restaurant has rejected your request',
            'Your dine-out request <br />has been accepted','Your dine-out request <br />has been accepted'];
        var ns= ['','Hold tight. We\'ll be in touch soon','Hold tight. We\'ll be in touch soon','','Check in with the host upon arrival at the restaurant','Check in with the host upon arrival at the restaurant'];
        var status=data.status;
        var rtz = Number(data.tz);
        var dst = Number(data.dst);

        rtz=((rtz+dst)*3600)*1000;

        var date = data.date;
        date=date.split(' ');
        date=date.join('T');
        date = new Date(date);

        var now = new Date();
        tord = date.getTime();
        tnow = now.getTime()+rtz;
        tcls = tord+7200000;
        var cont,title,tb;

        cont=document.createElement('div');
        cont.style.cssText='height:100%; position:relative; width:100%;';

        var layerTop=document.createElement('div');
        layerTop.style.cssText='display:inline-block; position:relative; width:100%; min-height:30%;';
        var layerMid=document.createElement('div');
        layerMid.style.cssText='display:inline-block; position:relative; width:100%; min-height:30%;';
        var layerBtm=document.createElement('div');
        layerBtm.style.cssText='display:inline-block; position:relative; width:100%; min-height:30%;';

        if(status<6 && status!=3){

            // If Order Time Still not in NOW range, ORDER NOT STARTED, TIME IS NOT COME
            if(tord>tnow) {

                info = cFnActiveOrderInfoPane({
                    time: data['date'], resto: data['resto_name'],
                    party: data.party['size'], started: false,
                    passed: false, completed: false
                });
                stat = cFnActiveOrderStatusPane(s[status]);

                layerTop.appendChild(info);
                layerTop.appendChild(stat);

                title=document.createElement('div');
                title.className='brand centered black full-width';
                title.textContent=ns[status];
                layerMid.appendChild(title);

            }
            // If Order time did't yet come
            else
            {
                title=document.createElement('div');
                title.className='brand centered black full-width';
                title.textContent=ns[status];

                // That means that USER EAT MEALS but hour after order datetime is not passed
                if(tnow<tcls && status>3){

                    info=cFnActiveOrderInfoPane({time:data['date'],resto:data['resto_name'],party:data.party['size'],started:true,passed:false,completed:false});
                    layerTop.appendChild(info);
                    layerTop.appendChild(title);

                }
                // Now time there is greater than ORDER TIME + 1 Hour
                else if(tnow>=tcls && status>3) {

                    info=cFnActiveOrderInfoPane({time:data['date'],resto:data['resto_name'],party:data.party['size'],started:true,passed:true,completed:false});
                    layerTop.appendChild(info);
                    //layerTop.appendChild(title);

                } else {

                    info = cFnActiveOrderStatusPane('This order is expired');
                    layerTop.appendChild(info);

                }


                tb=document.createElement('button');
                tb.className='cine_fn_color_btn half-width';
                tb.textContent='Make new reservation';
                tb.style.width='224px';
                tb.addEventListener('click',function(e){cineFnOrderCloseNotToPay(e,data.id);});

                layerMid.appendChild(tb);


            }

        } else if(status>5 && status<8) {

            info=cFnActiveOrderInfoPane({time:data['date'],resto:data['resto_name'],party:data.party['size'],started:false,passed:false,completed:true});
            layerTop.appendChild(info);

            tb=document.createElement('button');
            tb.className='cine_fn_color_btn half-width';
            tb.textContent='Pay my bill';
            tb.style.width='224px';
            tb.addEventListener('click',function(e){alert('Application got error')});

            layerMid.appendChild(tb);

        } else if(status==3){

            info = cFnActiveOrderStatusPane(s[status]);
            layerTop.appendChild(info);

            tb=document.createElement('button');
            tb.className='cine_fn_color_btn half-width';
            tb.textContent='Make new reservation';
            tb.style.width='224px';
            tb.addEventListener('click',function(e){cineFnOrderCloseNotToPay(e,data.id);});

            layerMid.appendChild(tb);

        }


        // Contact VOILA frame
        tbl = document.createElement('table');
        tbl.className='full-width lefted font18';
        tr = tbl.insertRow();
        tr.className='cine_list_item cine-list-small';
        cTableStruct(tr,{type:'cell',class:'half-width centered',plaintext:'Call us',height:64});
        cTableStruct(tr,{type:'cell',class:'half-width lefted',plaintext:'(347) 766-8795'});
        tr = tbl.insertRow();
        tr.className='cine_list_item cine-list-small';
        cTableStruct(tr,{type:'cell',class:'half-width centered',plaintext:'Email',height:64});
        cTableStruct(tr,{type:'cell',class:'half-width lefted',plaintext:'Voila Support'});


        tbl=cineBoxLabelFrame({title:'Contact Voila',elems:tbl,notpad:true});
        layerBtm.appendChild(tbl);
        cont.appendChild(layerTop);
        cont.appendChild(layerMid);
        cont.appendChild(layerBtm);
        return cont;

    }

    function cFnAOPartnerRestaurant(data){

        var s= ['Your order has not sent to restaurant yet',"Your dine-out request <br /> has been sent",
            'Restaurant now reviewing <br /> your order','Restaurant has rejected your request',
            'Your dine-out request <br />has been accepted'];
        var ns= ['','Hold tight. We\'ll be in touch soon','Hold tight. We\'ll be in touch soon','','Check in with the host upon arrival at the restaurant',''];

        var cont,z1,z2,z3,tbl,tbl2,tr,td,tb;

        var status = Number(data.status);
        var price = Number(data.price);
        var tax = Number(data.tax);
        var rtz = Number(data.tz);
        var partner = Number(data.resto_partner);
        var dst = Number(data.dst);

        rtz=((rtz+dst)*3600)*1000;

        var date = data.date;
        date=date.split(' ');
        date=date.join('T');
        date = new Date(date);
        var now = new Date();

        tord = date.getTime();
        tnow = now.getTime()+rtz;
        tcls = tord+7200000;

        cont=document.createElement('div');
        cont.style.cssText='height:100%; position:relative; width:100%;';

        //hgt= c.clientHeight;
        //mht=(Math.floor(hgt/3))-1;

        z1=document.createElement('div');
        z1.style.cssText='display:inline-block; position:relative; width:100%; min-height:30%;'; //min-height:'+(mht+mht)+'px;
        z1.setAttribute('id','c_aorder');
        //z2=document.createElement('div');
        //z2.style.cssText='display:inline-block; position:relative; width:100%; min-height:'+mht+'px;';
        //z2.setAttribute('id','c_aorder_b');
        z3=document.createElement('div');
        z3.style.cssText='display:inline-block; position:relative; width:100%; min-height:30%;';
        z3.setAttribute('id','c_order_h');

        tbl = document.createElement('table');
        tbl.className='full-width lefted font18 margin-t20 ';

        tbl2 = document.createElement('table');
        tbl2.className='full-width lefted font18 ';

        // > > > > > > > > > > > > > > > > > > > >
        // Status is lesser than any closing status
        // > > > > > > > > > > > > > > > > > > > >

        if(status<6){

            if(tord>tnow){

                info=cFnActiveOrderInfoPane({time:data['date'],resto:data['resto_name'],party:data.party['size'],started:false,passed:false,completed:false});
                stat=cFnActiveOrderStatusPane(s[status]);

                tr = tbl.insertRow(0);
                td = tr.insertCell(-1);
                td.style.textAlign='center';
                td.appendChild(info);

                tr = tbl.insertRow(1);
                td=cTableStruct(tr,{type:'cell',height:8});

                tr = tbl.insertRow(2);
                td=cTableStruct(tr,{type:'cell',class:'centered',domobj:stat});

                if(status!=3)
                {
                    tr = tbl2.insertRow(0);
                    td=cTableStruct(tr,{type:'cell',class:'navbar-brand',htmltext:'Next Steps'});
                    td.style.cssText='padding-left:0px !important; color:#333333;';
                    tr = tbl2.insertRow(1);
                    td=cTableStruct(tr,{type:'cell',class:'centered',plaintext:ns[status]});
                    tr = tbl2.insertRow(2);
                    td=cTableStruct(tr,{type:'cell',height:16});
                }

                next = cineBoxLabelFrame({title:false,elems:tbl2,color:'f8f8f8'});

                tr = tbl.insertRow(3);
                td=cTableStruct(tr,{type:'cell',class:'centered',domobj:next});

            } else if(status!=3) {

                if(tnow<tcls){

                    info=cFnActiveOrderInfoPane({time:data['date'],resto:data['resto_name'],party:data.party['size'],started:true,passed:false,completed:false});
                    tr = tbl.insertRow(0);
                    td=cTableStruct(tr,{type:'cell',class:'centered',domobj:info});

                } else {

                    info=cFnActiveOrderInfoPane({time:data['date'],resto:data['resto_name'],party:data.party['size'],started:true,passed:true,completed:false});
                    tr = tbl.insertRow(0);
                    td=cTableStruct(tr,{type:'cell',class:'centered',domobj:info});

                }

                tr = tbl2.insertRow();
                td=cTableStruct(tr,{type:'cell',height:4});
                tr = tbl2.insertRow();
                td=cTableStruct(tr,{type:'cell',class:'centered',htmltext:'<div class="navbar-brand" style="width:100% !important; color:#333333; max-width:320px !important; padding:10px 0 0 0; !important;"> Close your check </div>'});
                tr = tbl2.insertRow();
                td=cTableStruct(tr,{type:'cell',height:4});
                // Button for close

                tb=document.createElement('button');
                tb.className='cine_fn_color_btn half-width';
                tb.textContent='Pay my bill';
                tb.style.width='224px';
                tb.addEventListener('click',function(e){cineFnOrderQuery(e,this)}.bind(data.id));

                tr = tbl2.insertRow();
                td=cTableStruct(tr,{type:'cell',class:'centered',domobj:tb});
                next = cineBoxLabelFrame({title:false,elems:tbl2,color:'f8f8f8'});

                tr = tbl.insertRow(-1);
                td=cTableStruct(tr,{type:'cell',class:'centered',domobj:next});

            } else {

                // --> Order rejected by restaurant
                if(status==3){
                    tr = tbl2.insertRow();
                    cTableStruct(tr,{type:'cell',htmltext:'<h2> Sorry </h2>'});
                    tr = tbl2.insertRow();
                    cTableStruct(tr,{type:'cell',class:'padded',plaintext:'That order rejected by restaurant'});
                    tr = tbl2.insertRow();
                    cTableStruct(tr,{type:'cell',height:24});

                    next = cineBoxLabelFrame({title:false,elems:tbl2,color:'transparent'});
                    tr = tbl.insertRow(-1);
                    cTableStruct(tr,{type:'cell',class:'centered',domobj:next});

                    // Button for user AGREE rejection and start New Order
                    tb=document.createElement('button');
                    tb.className='cine_fn_color_btn half-width';
                    tb.textContent='Start another order';
                    tb.style.width='224px';
                    tb.addEventListener('click',function(e){cineFnOrderRejectedQuery(e,this)}.bind(data.id));

                    tr = tbl2.insertRow();
                    cTableStruct(tr,{type:'cell',class:'centered',domobj:tb});
                }

            }

        }
        // > > > > > > > > > > > > > > > > > > > > >
        // Status is in between closed and finalized
        // > > > > > > > > > > > > > > > > > > > > >
        else if(status>5 && status<8) {

            info=cFnActiveOrderInfoPane({time:data['date'],resto:data['resto_name'],party:data.party['size'],started:false,passed:false,completed:true});

            tr = tbl.insertRow();
            td=cTableStruct(tr,{type:'cell',class:'centered',domobj:info});

            tr = tbl2.insertRow();
            td=cTableStruct(tr,{type:'cell',class:'centered',htmltext:'<div class="navbar-brand" style="width:100% !important; color:#333333; max-width:320px !important; padding:10px 0 0 0; !important;"> Please close your check </div>'});
            tr = tbl2.insertRow();
            td=cTableStruct(tr,{type:'cell',height:8});
            // Button for close

            tb=document.createElement('button');
            tb.className='cine_fn_color_btn half-width';
            tb.textContent='Pay my bill';
            tb.style.width='224px';
            tb.addEventListener('click',function(e){cineFnOrderQuery(e,this)}.bind(data.id));

            tr = tbl2.insertRow();
            td=cTableStruct(tr,{type:'cell',class:'centered',domobj:tb});

            tc='<h3 style="margin-top:8px; margin-bottom:8px;">Too busy to close your check right now?</h3><i>Don\'t worry. Voila will close your bill automatically in one hour.</i>';
            tc=cineBoxLabelFrame({title:false,elems:tc,color:'f8f8f8'});
            tr = tbl2.insertRow();
            td=cTableStruct(tr,{type:'cell',class:'centered',domobj:tc});

            next = cineBoxLabelFrame({title:false,elems:tbl2,color:'f8f8f8'});
            tr = tbl.insertRow(-1);
            td=cTableStruct(tr,{type:'cell',class:'centered',domobj:next});

        }

        z1.appendChild(tbl);

        // Contact VOILA frame
        tbl = document.createElement('table');
        tbl.className='full-width lefted font18';
        tr = tbl.insertRow();
        tr.className='cine_list_item cine-list-small';
        cTableStruct(tr,{type:'cell',class:'half-width centered',plaintext:'Call us',height:64});
        cTableStruct(tr,{type:'cell',class:'half-width lefted',plaintext:'(347) 766-8795'});
        tr = tbl.insertRow();
        tr.className='cine_list_item cine-list-small';
        cTableStruct(tr,{type:'cell',class:'half-width centered',plaintext:'Email',height:64});
        cTableStruct(tr,{type:'cell',class:'half-width lefted',plaintext:'Voila Support'});


        tbl=cineBoxLabelFrame({title:'Contact Voila',elems:tbl,notpad:true});
        z3.appendChild(tbl);

        cont.appendChild(z1);
        cont.appendChild(z3);
        return cont;
    }

    function cFnActiveOrderInfoPane(d){

        var o, r, t, c, time, pty;
        var mt=['','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        ti=cTimeObject(d.time);

        date=new Date();
        cday=date.getUTCDate();
        ndate=date.getTime()+86400000;
        ndate=new Date(ndate);
        nday=ndate.getUTCDate();

        if(ti.d==cday){
            time='Today at '+cineTimeNormalize(ti.hour,ti.minute);
        } else if (ti.d==nday){
            time='Tomorrow at '+cineTimeNormalize(ti.hour,ti.minute);
        } else {
            time=ti.day+' '+mt[ti.m]+' at '+cineTimeNormalize(ti.hour,ti.minute);
        }

        o=document.createElement('div');
        o.style.cssText='position:relative; background-color:rgb(0,195,167); margin:10px 0 10px 0; border-radius:4px; padding:8px; min-height:32px; width:96%; display:inline-block; color:#ffffff;';

        r=document.createElement('div');
        r.style.cssText='display:block; width:100%; height:32px; padding:4px 0 8px 0; font-size:22px; text-align:left; border-bottom:1px dotted #f8f8f8;';
        r.textContent=d.resto;

        t = document.createElement('div');
        c = document.createElement('div');

        if(d.started){
            r.style.cssText='display:block; width:100%; height:32px; padding:4px 0 8px 0; font-size:22px; text-align:left; border-bottom:1px dotted #f8f8f8;';
            r.textContent=d.resto;
            t.style.cssText = 'display:inline-block; width:100%; height:32px; padding:8px 0 4px 0; font-size:22px; text-align:left; vertical-align:top;';
            t.textContent = 'Your reservation has started';
            if(d.passed)
                t.textContent = 'Reservation has already passed';

        } else if(d.completed) {
            r.style.cssText='display:block; width:100%; min-height:32px; padding:4px 0 8px 0; font-size:22px; text-align:center; line-height:normal;';
            r.innerHTML='We hope you enjoyed your meal at '+d.resto;
        } else {
            r.style.cssText='display:block; width:100%; height:32px; padding:4px 0 8px 0; font-size:22px; text-align:left; border-bottom:1px dotted #f8f8f8;';
            r.textContent=d.resto;
            t.style.cssText = 'display:inline-block; width:80%; height:32px; padding:8px 0 4px 0; font-size:22px; text-align:left; vertical-align:top;';
            t.textContent = time;
            c.style.cssText='display:inline-block; width:20%; height:32px; padding:8px 0 4px 0; font-size:22px; text-align:right !important; vertical-align:top;';
            cd=document.createElement('span');
            cd.setAttribute('id','c_party');
            cd.style.width='100%';
            cd.className='c_party';
            cd.style.textAlign='right';
            cd.innerHTML='&nbsp; '+ d.party;
            c.appendChild(cd);
        }

        o.appendChild(r);
        o.appendChild(t);
        o.appendChild(c);

        return o;

    }

    function cFnActiveOrderStatusPane(t){

        o=document.createElement('div');
        o.style.cssText='position:relative; background-color:rgba(0, 195, 167,0.9); margin:10px 0 10px 0; border-radius:4px; padding:8px; min-height:32px; width:96%; display:inline-block; color:#ffffff;';

        r=document.createElement('div');
        r.style.cssText='display:block; width:100%; min-height:32px; padding:6px 0 4px 0; font-size:22px; text-align:center;';
        r.innerHTML=t;
        o.appendChild(r);

        return o;
    }

    function cTableStruct(t,p){

        if(typeof p==='object') {
            var e;

            if (p.type == 'cell') {
                e = t.insertCell(-1);
                if (p.htmltext) {
                    e.innerHTML = p.htmltext;
                }
                else if (p.plaintext) {
                    e.textContent = p.plaintext;
                }
                if (p.domobj) {
                    e.appendChild(p.domobj);
                }
            } else {
                e = t.insertRow();
            }

            if (typeof p === 'object') {
                if (p.ident) {
                    e.setAttribute('id', p.ident);
                }
                if (p.class) {
                    e.className = p.class;
                }
                if (p.height) {
                    e.style.height = p.height + 'px';
                }
            }

            return e;
        }
        return false;
    }

    function cFnRestoOrderActiveAlign(){

        var l=cFscrOrdBox.style.left;
        l=cineBoxFnCoordPxRem(l);
        //w=cFscrOrdW/100*90;
        w=310;
        if(w>288){
            w=288;
        }
        la=Math.abs(l-(w/2));
        no=Math.abs(Math.floor(la/(w-20)));

        if(no>0)
            cFscrOrdBox.style.left = -(no*(w-20))+'px';
        else
            cFscrOrdBox.style.left = -5+'px';
    }

    function cFnRestoOrderActiveList(o,x,y){

        var l=cFscrOrdBox.style.left;
        l=cineBoxFnCoordPxRem(l);

        tCursorW=Math.abs(tCursorW);
        //wn=Math.round(cFscrOrdW/100*90);
        wn=310;
        //if(w>288){
        //    w=288;
        //}
        wl=wn-20;

        if(tCursorDx>x){

            w=cineBoxFnCoordPxRem(o.style.width);
            la=Math.abs(l);

            if(tCursorW>50){

                lo = la%wl;
                no = Math.abs(Math.floor(la/wl));

                if(lo>150){
                    if(no>0){
                        no++;
                        o.style.left = -(no*wl)+'px';
                    } else
                        o.style.left = -wl+'px';
                    tCursorW=0;
                } else {
                    l-=tCursorW/10;
                    o.style.left = l + 'px';
                    if(l<(-(w-(wn-30))))
                        o.style.left = -(w-wn) + 'px';
                }

            } else {
                l-=tCursorW/10;
                o.style.left = l + 'px';

                if(l<(-(w-(wn-30))))
                    o.style.left = -(w-wn) + 'px';
            }

        } else if (tCursorDx<x){
            if(l<40){

                la=Math.abs(l);

                if(tCursorW>50){

                    lo = wn-(la%wl);
                    no = Math.abs(Math.ceil(la/wl));
                    if(lo>150){
                        if(no>0){
                            no--;
                            o.style.left = -(no*wl)+'px';
                            tCursorW=0;
                        } else
                            o.style.left='-'+5+'px';

                    } else {
                        l+=tCursorW/10;
                        o.style.left = l + 'px';
                    }

                } else {
                    l+=tCursorW/10;
                    o.style.left = l + 'px';
                }
            } else {
                o.style.left=l + 'px';
            }
        } else {

        }

    }

    function cTouchStart(e){
        tCursorH=tCursorL=0;
        var touches = e.changedTouches;
        e.preventDefault();
        for(var t=0;t<touches.length;t++){
            cTouches[touches[t].identifier]={pageY:touches[t].pageY,pageX:touches[t].pageX};
        }
    }

    function cTouchMove(e,o,f){
        var touches = e.changedTouches;
        e.preventDefault();
        var dy=0,dx=0,ny=0,y=0,nx=0,x=0;
        for(var t=0;t<touches.length;t++){
            var tc=cTouches[touches[t].identifier];
            y=tc.pageY;
            x=tc.pageX;
            ny=touches[t].pageY;
            nx=touches[t].pageX;
            dy=Math.abs(ny-y);
            dx=Math.abs(nx-x);
        }
        tCursorH+=dy-tCursorL;
        tCursorW+=dx-tCursorM;
        tCursorL=dy;
        tCursorM=dx;

        f(o,nx,ny);

        tCursorDy=ny;
        tCursorDx=nx;
    }
    // *******************************************************
    // -------------------------------------------------------
    // | RESTAURANT ORDER LIST QUERY AND OPERATIONS
    // -------------------------------------------------------
    // *******************************************************
    function cineBoxFnRestoOrderList(e,r){

        //var c=new cineBox();
        var id=r.order;
        var g=false;

        if(typeof cineFnCartStatus!=='undefined'){
            if(document.getElementById(cineFnCartStatus)==null){
                g=true;
            }
        } else
            g=true;

        if(g){

            XWF.connQueryObject = {query:"restoorder", type:"getorderlist"};
            XWF.connQueryData = {
                ident:id,
                data:r.key,
                user:0,
                userkey:""
            };

            XWF.send({
                navigationBar:true,
                waitCallback:true,
                raiseWindow:true,
                raiseWindowID:"cart",
                windowClass:'theme',
                onComplete:cineBoxFnRestoOrderListRender,
                changeTitle:"Your order",
                windowTitle:"Restaurant Menu"
            });
        }

    }

    function cineBoxFnRestoOrderListRender(data){

        var c=data.content;
        var obj=data.data;

        var ec=null;

        if(obj.success)
        {
            cineFnCartStatus="cart";

            // Fills cineFnCartObject with CART items info -----------------
            cineFnCartObject={items:obj.result,order:obj.order,key:obj.key};

            // Filling standart VARIABLE for put items into order function
            ti = cTimeObject(obj.date+' '+obj.time);
            if(cineFnTimeObject==null)
                cineFnTimeObject={};
            cineFnTimeObject['day'] = ti.day;
            cineFnTimeObject['month'] = ti.month;
            cineFnTimeObject['year'] = ti.year;
            cineFnTimeObject['hour'] = ti.hour;
            cineFnTimeObject['minute'] = ti.minute;
            cineFnTimeObject['h'] = ti.h;
            cineFnTimeObject['m'] = ti.i;
            cineFnTimeObject['party'] = obj.party['size'];

            ec=cineBoxFnRestoOrderItemList(obj.result,obj.order,obj.key,true,{restaurant:obj.resto,restaurant_name:obj.resto_name});

            if(ec!=null)
                var cr=cineBoxLabelFrame({elems:ec,title:'Ordered Items'});

            cr.className += ' fadeInDown';
            c.appendChild(cr);

            // Add or Remove Discount button


            redeemBtn = document.createElement('button');
            redeemBtn.className = 'cine_fn_light_btn fadeInDown';
            redeemBtn.style.display='none';
            if(Number(obj['discount_id'])>0)
            {
                redeemBtn.style.display='none';
            }
            redeemBtn.textContent = 'Redeem Promo Code';
            redeemBtn.addEventListener('click', function(e) {
                cineBoxFnRedeemPromoCode(e)
            });
            c.appendChild(redeemBtn);



            ec=cineBoxFnRestoOrderItemSumm(obj.price,obj.tax,0,obj.discount,obj.discount_type);
            ec=cineBoxLabelFrame({elems:ec,title:'Order summary'});
            ec.className += ' fadeInDown';
            c.appendChild(ec);

            var eb=document.createElement('a');
            eb.className='cine_fn_color_btn fadeInDown';
            eb.textContent='Order checkout';
            eb.style.cssText = 'margin-top:20px !important; width:90%;';
            eb.addEventListener('click',function(ev){cineBoxFnRestoOrderCheckout(ev,this.id,this.win,this.key)}.bind({id:obj.order,win:data.window,key:obj.key}));
            c.appendChild(eb);

        } else {

            var el=document.createElement('div');
            el.className='';
            el.innerHTML='<h3> Your cart is empty at this time </h3>';
            el.style.cssText = 'margin-top:20px !important;';
            c.appendChild(el);

        }

    }

    // Create list of ordered items with delete buttons
    function cineOrderItemsToObject(o){
        var item_id,main_id,name,description,quantity,price,total,side_item,sides_total,sides_total_text,k,i,r;
        r=[];
        for(item_id in o) {
            if (o.hasOwnProperty(item_id)) {
                cur=o[item_id];

                itm=Object.keys(cur);
                len=itm.length;
                for(i=0;i<len;i++){
                    if(itm[i]!='info' && itm[i]!='pcard'){
                        main_id=itm[i];
                    }
                }

                obj=cur[main_id];
                name=obj.n;
                description=obj.d;
                quantity=parseInt(obj.q);
                price=Number(obj.p);

                sides_total = 0;
                sides_total_text = "";
                side_item=[];
                sides=obj.s;

                if(sides && typeof sides === 'object'){

                    for(var s in sides){
                        if(sides.hasOwnProperty(s)) {
                            sc = sides[s];

                            sub_name=sc['n'];
                            sub_quantity=Number(sc['q']);
                            sub_price=0;
                            sub_price_text="";

                            sub_price_main_txt="";
                            sub_price_sub_txt="";

                            p1 = Number(sc['p']);
                            p2 = Number(sc['p2']);

                            if (p1 > 0) {
                                sub_price += p1;
                                sub_price_main_txt="$ "+p1;
                            }
                            else if (p1==0)
                                sub_price_main_txt="Free";

                            sub_variant_id=parseInt(sc['i']);
                            sub_variant=sc['v'];
                            if(sub_variant.length>0 || sc['p2'] != null){
                                if (p2 > 0) {
                                    sub_price += p2;
                                    sub_price_sub_txt="$ "+p2;
                                }
                                sub_name += ' '+sub_variant;
                            } else {
                                sub_variant=null;
                                p2=null;
                            }

                            if (sub_price > 0)
                                sub_price_text = '$ ' + sub_price;

                            sides_total+=sub_price;
                            side_item.push(
                                {
                                name:sub_name,price:sub_price,
                                price_text:sub_price_text,mainPrice:p1,subPrice:p2,
                                mainPriceText:sub_price_main_txt,subPriceText:sub_price_sub_txt,
                                quantity:sub_quantity,variant:sub_variant,variant_id:sub_variant_id
                                });
                        }
                    }
                    sides_total_text='$ ' +sides_total;

                } else {

                    side_item=null;

                }

                total=price+sides_total;
                r.push(
                    {
                    idInOrder:item_id,idInSystem:main_id,name:name,
                    description:description,price:price,priceText:price.toFixed(2),quantity:quantity,
                    sideItems:side_item,totalPrice:total,totalPriceText:'$ '+total.toFixed(2),
                    totalPriceSides:sides_total,totalPriceSidesText:sides_total_text
                    }
                );
            }
        }

        return (r.length>0)?r:false;
    }

    function cineBoxFnRestoOrderItemList(r,id,key,fn,attr)
    {

        var i=0,k,cur,itm,elm,ec,et,eb,er;

        if(typeof attr !== 'undefined'){
            closed = (attr.closed);
            restaurant = (attr.restaurant);
            restaurant_name = attr.restaurant_name;
            show_add_button = attr.showAddButton;
        } else {
            closed = false;
            restaurant = false;
            restaurant_name = null;
            show_add_button = false;
        }


        if(typeof fn==='undefined')fn=true;

        ec=document.createElement('table');
        ec.className='cine_fn_rol_i';
        ec.setAttribute('id','c_order_i_'+id);

        item=cineOrderItemsToObject(r);

        addRows=1;

        if(item){

            len=item.length;
            for(i=0;i<len;i++){

                its=item[i];
                mainRow=document.createElement('tr');
                mainRow.setAttribute('id','cie_'+its.idInOrder);

                mainCell=document.createElement('td');
                mainRow.appendChild(mainCell);
                ec.appendChild(mainRow);

                itemTable=document.createElement('table');
                itemTable.className='full-width';

                itemRow=document.createElement('tr');
                itemRow.className='cine_fn_rol_i_tr';

                itemCell=document.createElement('td');
                itemCell.className='cine_fn_rol_i_td';
                itemCell.innerHTML='<h4>'+its.name+'</h4>';
                itemRow.appendChild(itemCell);

                itemCell=document.createElement('td');
                itemCell.className='cine_fn_rol_i_tq';
                itemCell.setAttribute('id','ciq_'+its.idInOrder);
                itemCell.innerHTML='x'+its.quantity+'';
                itemRow.appendChild(itemCell);

                itemCell=document.createElement('td');
                itemCell.className='cine_fn_rol_i_tp';
                itemCell.textContent='$ '+its.priceText;
                itemRow.appendChild(itemCell);

                if(fn){
                    addRows=2;
                    itemCell=document.createElement('td');
                    itemCell.className='cine_fn_rol_i_tb';
                    eb=document.createElement('a');
                    eb.className='cine_fn_color_btn cine_table_btn';
                    eb.innerHTML='<div class="c-btn-delete"></div>';
                    eb.addEventListener('click',function(e){cineBoxFnRestoOrderIdelete(e,this.id,this.nid,this.oid,this.key)}.bind({id:its.idInOrder,nid:its.idInSystem,oid:id,key:key}));
                    itemCell.appendChild(eb);
                    itemRow.appendChild(itemCell);
                }

                itemTable.appendChild(itemRow);

                if(its.sideItems!=null)
                {

                    sitm=its.sideItems;
                    slen=sitm.length;

                    for(k=0;k<slen;k++){

                        itemRow=document.createElement('tr');
                        itemRow.className='cine_fn_rol_i_tr';

                        itemCell=document.createElement('td');
                        itemCell.setAttribute('colspan','2');

                        sits=sitm[k];
                        container=document.createElement('div');
                        container.className='cine_fn_rol_i_subi';
                        container.textContent=sits.name;
                        if(sits.price>0) {
                            extendcontainer = document.createElement('span');
                            container.appendChild(extendcontainer);
                        }
                        itemCell.appendChild(container);
                        itemRow.appendChild(itemCell);

                        itemCell=document.createElement('td');
                        itemCell.setAttribute('colspan',addRows);

                        pricecontainer=document.createElement('div');
                        pricecontainer.textContent=sits.price_text;
                        pricecontainer.className='cine_fn_rol_i_subp';

                        itemCell.appendChild(pricecontainer);
                        itemRow.appendChild(itemCell);

                        itemTable.appendChild(itemRow);

                    }

                }

                mainCell.appendChild(itemTable);

            }

        }

        if((!cineBoxFnIsRestoMenuOnScreen() && !closed && restaurant) || show_add_button)
        {
            mainRow=document.createElement('tr');
            mainCell=document.createElement('td');
            mainRow.appendChild(mainCell);

            eb=document.createElement('button');
            eb.className='full-width cine_fn_frame_btn';
            eb.style.cssText = "color:#AAAAAA; border-top:1px dotted #dedede; margin-top:10px;";
            eb.innerHTML=' Add more items ';
            eb.addEventListener('click',function(e){cineBoxFnRestoOrder(e,this.r,this.rn);}.bind({r:restaurant,rn:restaurant_name}));
            mainCell.appendChild(eb);
            ec.appendChild(mainRow);
        }

        return ec;
        //return false;
    }

    function cineBoxFnRestoOrderItemListElem(o){

    }

    // Create a summary for item checkout
    function cineBoxFnRestoOrderItemSumm(p,t,u,d,dt)
    {

        $cine().order.countPrices(p,t,u,d,dt);

        var i=0;

        var ec=document.createElement('table');
        ec.className='cine_fn_rol_i_total font16 full-width';
        ec.setAttribute('id','cine_fn_order_total');

        var er=ec.insertRow(i);
        er.className='cine_fn_rol_i_total_s';
        var et=er.insertCell(0);
        et.textContent='Subtotal';
        et=er.insertCell(1);
        et.setAttribute('id','cine_fn_rol_itotal');
        et.className='cine_fn_rol_i_price';
        et.textContent='$ '+ $cine().order.price(astype.string);
        i++;

        er=ec.insertRow(i);
        er.className='cine_fn_rol_i_total_s';
        et=er.insertCell(0);
        et.textContent='Tax';

        et=er.insertCell(1);
        et.setAttribute('id','cine_fn_rol_ttotal');
        et.className='cine_fn_rol_i_price';
        et.textContent='$ ' + $cine().order.tax(astype.string);
        i++;

        if(typeof d !=='undefined'){

            if(d>0){
                er=ec.insertRow(i);
                er.className='cine_fn_rol_i_total_s';
                et=er.insertCell(0);
                et.textContent='Discount';
                et=er.insertCell(1);
                et.className='cine_fn_rol_i_price';
                et.setAttribute('id','cine_fn_rol_dtotal');

                et.textContent='$'+ $cine().order.discount(astype.string);
                i++;
            }

        }

        if(typeof u !=='undefined'){

            if(u>0) {
                er = ec.insertRow(i);
                er.className = 'cine_fn_rol_i_total_s';
                et = er.insertCell(0);
                et.textContent = 'Customer tip';
                et = er.insertCell(1);
                et.className = 'cine_fn_rol_i_price';
                et.setAttribute('id', 'cine_fn_rol_utotal');

                et.textContent = '$' + $cine().order.tip(astype.string);
                i++;
            }

        }

        er=ec.insertRow(i);
        er.style.cssText='border-top:1px solid #cccccc; padding-top:4px;';
        er.className='cine_fn_rol_i_total_s';

        et=er.insertCell(0);
        et.innerHTML='<h4>Total</h4>';
        et=er.insertCell(1);
        et.setAttribute('id','cine_fn_rol_ototal');
        et.className='cine_fn_rol_i_price';
        et.innerHTML='<h4>$ '+$cine().order.total(astype.string)+'</h4>';

        return ec;
    }

    var cPromoCodeTimeout;
    var cPromoCodeInfoField;

    // ------------------------------------
    // ------------------------------------
    //      REDEEM PROMOCODES
    // ------------------------------------
    // ------------------------------------

    function cineBoxFnRedeemPromoCode(e) {

        var modal=cOpenTinyModal(
            {buttonShow:true,buttonName:'Redeem Code',buttonCloseWindow:false,
            buttonEvent:cineBoxFnQueryPromoRedeem,appTitle:'Redeem Promo Code',
            buttonCancel:true,initiator:e.target});

        var formPC = document.createElement('form');
        formPC.className='cine_form_green';
        formPC.style.cssText='padding-top:20px;';

        label = document.createElement('label');
        label.textContent = 'Type-in your promo code';

        input = document.createElement('input');
        input.setAttribute('type','text');
        input.setAttribute('value','');
        input.addEventListener('input',function(e){cineBoxFnCheckPromoCode(e)});

        formPC.appendChild(label);
        formPC.appendChild(input);

        var fieldPC = document.createElement('div');
        fieldPC.className='cine_promocode';
        cPromoCodeInfoField=fieldPC;

        modal.content.appendChild(formPC);
        modal.content.appendChild(fieldPC);
    }

    function cineBoxFnQueryPromoRedeem(e){

        if(cinePromoCodeObject!=null)
        {
            data = JSON.stringify(cinePromoCodeObject);
            order= JSON.stringify({order:cineFnCartObject.order,key:cineFnCartObject.key});

            XWF.connQueryObject = {query:"restoorder", type:"putpromocode"};
            XWF.connEarlyTermination=false;
            XWF.connQueryData = {
                ident:order,
                data:data
            };

            $cine().userkey(XWF.connQueryData);

            XWF.send({
                waitCallback:true,
                onComplete:cineBoxFnDisplayPromoCodeRedeem
            });
        }

    }

    function cineBoxFnDisplayPromoCodeRedeem(data){

        var obj=data.data;

        if(obj.success){

            r=obj.result;

            if(r){

                button=cineFnCurrentModal.initiator;
                if(button!=null){
                    button.style.display='none';
                }

                cRemoveTinyModal();
                var table=document.getElementById('cine_fn_order_total');
                var summary=table.parentNode;
                cineFnClearDOM(summary);

                newSummary=cineBoxFnRestoOrderItemSumm(cOrderPrice.priceCash,cOrderPrice.taxPercent,cOrderPrice.tipPercent, r['discount'],r['discount_type']);
                summary.appendChild(newSummary);

            }

        } else {

            if(obj.hasOwnProperty('warn')){

                warn = document.createElement('h4');
                warn.textContent=obj['warn_msg'];
                cPromoCodeInfoField.appendChild(warn);

            }

        }

    }

    function cineBoxFnCheckPromoCode(e){

        var input=e.target;
        var text=input.value;
        var len=text.length;

        if(len>=6){

            clearTimeout(cPromoCodeTimeout);
            cPromoCodeTimeout=setTimeout(function(){cineBoxFnQueryPromoCode(this);}.bind(text),1000);

        } else {

            cinePromoCodeObject=null;
            cineFnClearDOM(cPromoCodeInfoField);
            amountLabel=document.createElement('h4');
            amountLabel.textContent='The promo code need to have at least 6 symbols';
            cPromoCodeInfoField.appendChild(amountLabel);

        }

    }

    function cineBoxFnQueryPromoCode(text){

        XWF.connQueryObject = {query:"restoorder", type:"getpromocode"};
        XWF.connQueryData = {
            ident:0,
            data:text,
            user:0
        };

        XWF.send({
            onComplete:cineBoxFnDisplayPromoCodeResult
        });

    }

    function cineBoxFnDisplayPromoCodeResult(data){

        cineFnClearDOM(cPromoCodeInfoField);
        var obj=data.data;

        if(obj.success){


            var expired;
            var notstarted;

            var r = obj.result;

            if(r){

                dateStart=cTimeObject(r.datefrom);
                dateEnd=cTimeObject(r.datedue);

                date=new Date();

                expired=(date.getDate() > dateEnd.d && date.getMonth()+1 >= dateEnd.m && date.getFullYear() >= dateEnd.y);
                notstarted=(date.getDate() < dateStart.d && date.getMonth()+1 <= dateEnd.m && date.getFullYear() <= dateEnd.y);



                if(!expired && !notstarted){

                    cinePromoCodeObject={code: r.code, name: r.name, type:Number(r.type),
                        value:Number(r.value), valuetype:Number(r.valuetype), active:Number(r.active),
                        dateStart:dateStart,dateEnd:dateEnd};

                    // IF ALL FONNA GOOD LETS OUTPUT PROMOCODE
                    if(cinePromoCodeObject.active==1){

                        amountLabel=document.createElement('h4');
                        amountLabel.textContent='off your order';
                        amountValue=document.createElement('h2');

                        dType = (cinePromoCodeObject.valuetype==0)?cinePromoCodeObject.value+'%':'$'+cinePromoCodeObject.value;
                        amountValue.textContent=dType;

                        cPromoCodeInfoField.appendChild(amountValue);
                        cPromoCodeInfoField.appendChild(amountLabel);

                    } else {

                        field=document.createElement('h4');
                        field.textContent='This promocode has already been redeemed';
                        cPromoCodeInfoField.appendChild(field);
                        cinePromoCodeObject=null;

                    }

                } else {

                    cinePromoCodeObject=null;

                }

                if(expired){
                    field=document.createElement('h4');
                    field.textContent='This promocode is expred';
                    cPromoCodeInfoField.appendChild(field);
                    cinePromoCodeObject=null;
                }

                if(notstarted){
                    field=document.createElement('h4');
                    field.innerHTML='This promo code is not valid until <br /> '+dateStart.year+' / '+dateStart.month+'/'+dateStart.day;
                    cPromoCodeInfoField.appendChild(field);
                    cinePromoCodeObject=null;
                }

            } else {

                field=document.createElement('h4');
                field.textContent='The promo code you entered is invalid';
                cPromoCodeInfoField.appendChild(field);
                cinePromoCodeObject=null;
            }

        } else {
            field=document.createElement('h4');
            field.textContent='The promo code you entered is invalid';
            cPromoCodeInfoField.appendChild(field);
            cinePromoCodeObject=null;
        }

    }

    function cineBoxFnRestoOrderIdelete(e,id,nid,oid,key){

        var data=JSON.stringify({order:oid,item:id,subitem:nid});

        XWF.connQueryObject = {query:"restoorder", type:"delitemfromorder"};
        XWF.connQueryData = {
            ident:oid,
            data:data,
            ext:key
        };

        $cine().userkey(XWF.connQueryData);

        XWF.send({
            waitCallback:true,
            onComplete:cineBoxFnRestoOrderIdeleteProc
        });

    }

    function cineBoxFnRestoOrderIdeleteProc(ret){

        var r=ret.data;
        var data=r.result;

        var elem= document.getElementById('cie_'+data.item);
        var elemq= document.getElementById('ciq_'+data.item);

        var ttlp= document.getElementById('cine_fn_rol_itotal');
        var ttlt= document.getElementById('cine_fn_rol_ttotal');
        var ttlo= document.getElementById('cine_fn_rol_ototal');

        if(data.q>0){
            elemq.innerHTML='x&nbsp;'+data.q;
        } else {
            elem.parentNode.removeChild(elem);
        }

        var pt=((data.p/100)*(8.75)).toFixed(2);
        var pto=Number(pt)+Number(data.p);

        ttlp.innerHTML='$ '+data.p.toFixed(2);
        ttlt.innerHTML='$ '+pt;
        ttlo.innerHTML='$ '+pto.toFixed(2);

    }

    function cineBoxFnRestoOrderCheckout(e,id,win,key){

        //t=win.id;
        //if(typeof e!=='undefined')
        //    var t=cineBoxFnDomObject(e.target);

        XWF.connQueryObject = {query:"restoorder", type:"getordercheckout"};
        XWF.connQueryData = {
            ident:id,
            data:"",
            adata:key
        };

        //$cine().userkey(XWF.connQueryData);
        var data=XWF.connQueryData;

        XWF.send({
            navigationBar:true,
            waitCallback:true,
            raiseWindow:true,
            raiseWindowID:"send-order",
            windowClass:'theme',
            onComplete:cineBoxFnRestoOrderRndrCheckout,
            changeTitle:'Order Checkout',
            data:function(){return $cine().userkeyupd(data);}
        });
    }

    function cineBoxFnRestoOrderRndrCheckout(data){

        var c=data.content;
        var obj=data.data;

        if(obj.success){

            var res=obj.result;
            var key=obj.shared;
            var i;

            ec=document.createElement('table');
            ec.className='cine_fn_rol_i';

                //for(var k in res){
                    //if(res.hasOwnProperty(k)){

                        //cur=res[k];
                        //itm=Object.keys(cur);

                        er=ec.insertRow(0);
                        er.className='cine_fn_rol_i_tr';
                        //er.setAttribute('id','cie_'+k);

                        /*
                        et=er.insertCell(0);
                        et.className='cine_fn_rol_i_td';
                        et.innerHTML='<h4>'+cur[itm[0]].n+'</h4>';


                        et=er.insertCell(1);
                        et.className='cine_fn_rol_i_tq';
                        et.setAttribute('id','ciq_'+k);
                        et.textContent='x '+cur[itm[0]].q+'';

                        et=er.insertCell(2);
                        et.className='cine_fn_rol_i_tp';
                        et.textContent='$ '+cur[itm[0]].p;
                        */
                        $cine().order.countPrices(obj.price,obj.tax,obj.tips,obj.discount,obj.discount_type);

                        et=er.insertCell(0);
                        et.className='cine_fn_rol_i_td';
                        et.innerHTML='<h4> Total </h4>';

                        et=er.insertCell(1);
                        et.className='cine_fn_rol_i_tp';
                        et.style.cssText='text-align:right !important;';
                        et.textContent='$ '+$cine().order.total(astype.string);
                    //}
                //}

            var fmo=cineBoxLabelFrame({title:'Order Summary',elems:ec});
            fmo.className += ' fadeInDown';
            c.appendChild(fmo);

            // Party-size
            var pts=obj.party;
            // Date and time

            var dat=cTimeObject(obj.date);
            var cdat=new Date();
            //console.log(dat);
            day=dat.day;
            mon=dat.month;
            yer=dat.year;
            cday=cdat.getDate();
            cmon=cdat.getMonth()+1;
            cyer=cdat.getFullYear();
            cday=(cday<10)?'0'+cday:cday;
            cmon=(cmon<10)?'0'+cmon:cmon;

            cds=Number(cyer+''+cmon+''+cday);
            ds=Number(yer+''+mon+''+day);

            if(cds>ds){
                dat=cTimeObject(cyer+'-'+cmon+'-'+cday+' '+dat.hour+':'+dat.minute+':'+dat.second);
            }

            var odat=cineMakeDays(dat,true,obj.id);

            var rh=dat.h;
            var m=dat.minute;

            cineFnOrderObject={id:obj.id,key:obj.shared,year:odat.year,month:odat.month,day:odat.day,hour:rh,minute:m,second:'00',party:Number(pts['size'])};

            ec=document.createElement('div');
            ec.className='cine_fn_rol_i cine_rol_i_dark c_datetime';
            ec.addEventListener('click',function(){cGetSetDate(this);}.bind(obj.id));

            i=document.createElement('div');
            i.className='c_date';
            i.setAttribute('id','c_date'+obj.id);
            i.textContent=odat.cday;
            ec.appendChild(i);

            i=document.createElement('div');
            i.className='c_time';
            i.setAttribute('id','c_time'+obj.id);
            i.textContent=cineTimeNormalize(dat.hour,dat.minute);
            ec.appendChild(i);

            i=document.createElement('div');
            i.className='c_party';
            i.setAttribute('id','c_party'+obj.id);
            i.textContent=pts['size'];
            ec.appendChild(i);

            // Append time and party
            var tmo=cineBoxLabelFrame({title:'Details',elems:ec,color:'#34495E'});
            tmo.className += ' fadeInDown';
            c.appendChild(tmo);

            er=document.createElement('textarea');
            er.style.cssText='resize:none; width:100%;';
            er.setAttribute('id','cine_ordertext'+obj.id);
            var txt=cineBoxLabelFrame({title:'Special Instructions',elems:er,color:'transparent',notpad:true});
            txt.className += ' fadeInDown';
            c.appendChild(txt);

            // Button
            eb=document.createElement('a');
            eb.className='cine_fn_color_btn fadeInDown';
            eb.innerHTML='Send order to restaurant';
            eb.style.cssText = 'margin-top:20px !important; width:90%;';
            eb.addEventListener('click',function(ev){cineBoxFnRestoOrderUserPush(ev,this.id,this.key)}.bind({id:obj.id,key:key}));
            c.appendChild(eb);

        }

    }

    function cineBoxFnRestoOrderUserPush(e,id,key){

        var otext=document.getElementById('cine_ordertext'+id);
        var odata=JSON.stringify({
            date:cineFnOrderObject.year+'-'+cineFnOrderObject.month+'-'+cineFnOrderObject.day,
            time:cineFnOrderObject.hour+':'+cineFnOrderObject.minute+':00',
            party:cineFnOrderObject.party});

        XWF.connQueryObject = {query:"restoorder", type:"putordercheckout"};
        XWF.connEarlyTermination=false;
        XWF.connQueryData = {
            ident:id,
            data:odata,
            adata:key,
            ext:otext.value
        };

        $cine().userkey(XWF.connQueryData);

        XWF.send({
            waitCallback:true,
            onComplete:cineBoxFnRestoOrderUserPushAfter
        });

    }

    function cineBoxFnRestoOrderUserPushAfter(data){

        var obj = data.data;

        if(obj.success)
        {

            //{"success":true,
            // "result":{"i":"126","k":"0837f1f1ab8ed09d616f45a6087f9724","p":"25.00","t":8.875,"u":20,"n":"Relevant Eatr","r":"1","d":"2015-01-09 21:15:00","s":9,"z":"-5"},
            // "message":"",
            // "order":"126"}

            cineFnOrderObject=null;
            var r=obj.result;
            var oid=obj['order'];
            var key=r['k'];
            var price=r['p'];
            var tax=r['t'];
            var restoname=r['n'];
            var resto=r['r'];
            var date=r['date'];
            var status=r['s'];

            if(window.localStorage!=='undefined')
            {
                var res={order:0,user:0,key:0};
                window.localStorage.setItem('userorder',JSON.stringify(res));
            }

            /// Populate main LIST of orders object
            if(cineFnOrderList==null)
                cineFnOrderList={};

            cineFnOrderList[oid]=r;

            // Add single object to push new order to screen
            var cineFnOrderNewE={};
            cineFnOrderNewE[oid]=r;
            if(cFscrOrdBox!=null)
                cFnRestoOrderActiveAdd(cineFnOrderNewE,true);
            else
                cFnRestoOrderActive(cineFnOrderList);

            // Reset actual restaurant
            cineResetRestaurant(false);

            // Hide Labels to choose another restaurant
            cFscrRestoFinderDisplay(false);

            // Remove all windows
            //cineWinCtrlRemAll();
            XWF.removewindows();
            // Clear cart
            cineBoxFnCartState(false);
            $cmenu.enable('orderstat');

        }

    }

    /* -----------------------------------------------
     * * * * * * * * * * * * * * * * * * * * * * * * *
     * RESTAURANT REJECTS ORDER
     * * * * * * * * * * * * * * * * * * * * * * * * *
    ----------------------------------------------- */
    function cineFnOrderRejectedQuery(e,id){

        XWF.connQueryObject = {query:"restoorder", type:"putuserorderreject"};
        XWF.connEarlyTermination=false;
        XWF.connQueryData = {
            ident:id,
            data:""
        };

        $cine().userkey(XWF.connQueryData);

        XWF.send({
            waitCallback:true,
            onComplete:cineFnOrderRejectAction
        });

    }

    function cineFnOrderRejectAction(data){

        var o = data.data;

        if(o.success) {

            delete cineFnOrderList[o.order];
            ks=Object.keys(cineFnOrderList);
            l=ks.length;

            if(cineCurrentView=='index') {

                oe=document.getElementById('c_ordelem'+o.order);
                if(oe!=null) {

                    oe.parentNode.removeChild(oe);
                    cFscrOrdBox.style.width = ((cFscrOrdW - 20) * l) + 20 + 'px';
                    cFscrOrdBox.style.left = 0;

                    if (l < 1)
                    {
                        $cmenu.disable('orderstat');
                        cFscrOrd.style.display = 'none';
                        cFscrHeader=document.getElementById('c_fscr_header');
                        cFscrRestoFinderDisplay(true);
                    }
                }

                XWF.closewindow();
            }
            else
                window.location='index.html';


        }

    }

    /* -----------------------------------------------
     * * * * * * * * * * * * * * * * * * * * * * * * *
     * USER FINALIZE NON PAYMENT ORDER
     * * * * * * * * * * * * * * * * * * * * * * * * *
     ---------------------------------------------- */
    function cineFnOrderCloseNotToPay(e,id){

        XWF.connQueryObject = {query:"restoorder", type:"putnotpaidorder"};
        XWF.connQueryData = {ident:id, data:""};
        XWF.connEarlyTermination = false;

        $cine().userkey(XWF.connQueryData);

        XWF.send({
            waitCallback:true,
            onComplete:cineFnOrderCloseNotToPayFinish
        });

    }

    function cineFnOrderCloseNotToPayFinish(data){

        var obj=data.data;

        if(obj.success){

            delete cineFnOrderList[obj.order];
            ks=Object.keys(cineFnOrderList);
            l=ks.length;

            if(cineCurrentView=='index') {

                oe=document.getElementById('c_ordelem'+obj.order);

                if(oe!=null) {

                    oe.parentNode.removeChild(oe);
                    cFscrOrdBox.style.width = ((cFscrOrdW - 20) * l) + 20 + 'px';
                    cFscrOrdBox.style.left = 0;

                    if (l < 1) {
                        $cmenu.disable('orderstat');
                        cFscrOrd.style.display = 'none';
                        cFscrHeader=document.getElementById('c_fscr_header');
                        cFscrRestoFinderDisplay(true);
                    }
                }

                XWF.closewindow();
            }
            else
                window.location='index.html';
        }

    }

    /* -----------------------------------------------
     * * * * * * * * * * * * * * * * * * * * * * * * *
     * USER TIPS AND FINALIZE PAYMENTS
     * * * * * * * * * * * * * * * * * * * * * * * * *
     ---------------------------------------------- */

    function cineFnOrderQuery(e,id){

        XWF.connQueryObject = {query:"restoorder", type:"getuserorderdata"};
        XWF.connQueryData = {
            ident:id,
            data:""
        };

        $cine().userkey(XWF.connQueryData);

        XWF.send({
            navigationBar:true,
            waitCallback:true,
            raiseWindow:true,
            raiseWindowID:"finish-order",
            windowClass:'theme',
            onComplete:cineFnOrderDisplay,
            changeTitle:'Order Checkout'
        });

    }

    function cineFnOrderDisplay(data){

        var c=data.content;
        var obj = data.data;

        if(obj.success){

            var r=obj.result;
            var status=Number(r.status);
            var price=Number(r.price);
            var tax=Number(r.tax);
            var tip=Number(r.tip);

            var t=$cine().order.coItems(r['order'],r.id,r.shared,false);
            t=$cine().helper.frameLabeled({elems:t,title:'Items in order'});

            var tdb=[];
            var actv={};
            actv['15']='cine_fn_color_btn';
            actv['20']='cine_fn_color_btn';
            actv['25']='cine_fn_color_btn';
            actv['30']='cine_fn_color_btn';

            if(tip==0){
                tip=20;
                actv['20']='cine_fn_color_btn cine_btn_active';
            } else {
                actv[tip]='cine_fn_color_btn cine_btn_active';
            }

            if(status<7) {
                for(i=1;i<5;i++){
                    tnum=(i+2)*5;
                    ti=document.createElement('button');
                    ti.setAttribute('id','ctip'+i);
                    ti.addEventListener('click',function(e){cineFnRadioSwitch(e,'ctip',this.i,this.k,this.p,this.bp,this.bt,this.id)}.bind({n:i,k:r.shared,p:tnum,bp:price,bt:tax,id:r.id}));
                    ti.className='ctip '+ actv[tnum];
                    ti.textContent=tnum+'%';
                    ti.style.width='44%';
                    tdb.push(ti);
                }
            }

            var ts=$cine().helper.frameLabeled({elems:tdb,title:'Tip amount'});
            ts.style.textAlign='center';

            var s=cineBoxFnRestoOrderItemSumm(price, tax, tip, r.discount, r.discount_type);
            s=$cine().helper.frameLabeled({elems:s,title:'Order summary'});

            var eb = document.createElement('span');
            if(status>3 && status<8){
                eb = document.createElement('button');
                eb.className = 'cine_fn_color_btn';
                eb.innerHTML = 'Pay the bill';
                eb.style.cssText = 'margin-top:20px !important; width:90%;';
                eb.addEventListener('click', function (ev) {
                    cineFnUserAcceptOrder(this.id)
                }.bind({id:r.id}));
            }

            c.appendChild(t);
            c.appendChild(ts);
            c.appendChild(s);
            c.appendChild(eb);

        }

    }

    function cineFnRadioSwitch(e,scope,n,key,p,bp,bt,i){

        var io=document.getElementById('cine_fn_rol_utotal');
        var ia=document.getElementById('cine_fn_rol_ototal');
        var items=document.getElementsByClassName(scope);
        var l=items.length;

        for(var k=0;k<l;k++){
            if(cineBoxFnReturnClass(items[k].className,'cine_btn_active')){
                items[k].className=scope+' '+'cine_fn_color_btn';
            }
        }

        e.target.className = scope+ ' cine_fn_color_btn cine_btn_active';

        if(io!=null){
            cOrderPriceCount(bp,bt,p);
            io.textContent='$ '+(cOrderPrice.tipCash).toFixed(2);
            ia.textContent='$ '+(cOrderPrice.totalCash).toFixed(2);
        }

        cineFnPushTipChange(false,i,p,key);

    }

    function cineFnPushTipChange(e,oid,prc,key){

        XWF.connQueryObject = {query:"restoorder", type:"putusertipamount"};
        XWF.connQueryData = {
            ident:oid,
            data:prc,
            ext:key
        };

        $cine().userkey(XWF.connQueryData);

        XWF.send({
            waitCallback:false
        });
    }

    function cineFnUserAcceptOrder(id){

        XWF.connQueryObject = {query:"restoorder", type:"putuserorderaccept"};
        XWF.connEarlyTermination=false;
        XWF.connQueryData = {
            ident:id
        };

        $cine().userkey(XWF.connQueryData);

        XWF.send({
            waitCallback:true,
            onComplete:cineFnUserAcceptOrderComplete
        });

    }

    function cineFnUserAcceptOrderComplete(data){

        var obj = data.data;

        if(obj.success){

            //{"success":true,
            // "result":{"i":125,"k":"04233ae735c9d5f4c6946c430c97f812","p":"14.00","t":8.875,"u":"20","n":"Relevant Eatr","r":"1","d":"2015-01-09 16:47:00","s":9,"z":"-5"},
            // "message":"Order successfully complete",
            // "completed":true,
            // "order":125}

            var r=obj.result;

            var z=document.getElementById('c_aorder');
            z.style.textAlign='center';

            if(z!=null){

                co=cineFnOrderList[obj.order];

                cineFnClearDOM(z);

                z.innerHTML='<h2 style="margin-top:64px;"> Thank you for dining at '+co['n']+' </h2>';

                eb = document.createElement('button');
                eb.className = 'cine_fn_color_btn';
                eb.innerHTML = 'Make new reservation';
                eb.style.cssText = 'margin-top:20px !important;';
                eb.addEventListener('click', function () {
                    if(cineCurrentView=='index')
                        XWF.closewindow();
                    else
                        window.location='index.html';
                });
                z.appendChild(eb);

                delete cineFnOrderList[obj.order];
                ks=Object.keys(cineFnOrderList);
                l=ks.length;

                oe=document.getElementById('c_ordelem'+obj.order);
                if(oe!=null){

                    oe.parentNode.removeChild(oe);
                    cFscrOrdBox.style.width=((cFscrOrdW-20)*l)+20+'px';
                    cFscrOrdBox.style.left=0;

                    if(l<1){

                        $cmenu.disable('orderstat');

                        cFscrOrd.style.display='none';
                        cFscrHeader=document.getElementById('c_fscr_header');
                        cFscrNoOrders=document.getElementById('c_fscr_noorders');
                        if(cFscrHeader!=null){
                            cFscrRestoFinderDisplay(true);
                        }
                        if(cineCurrentView=='order'){
                            cFscrNoOrders=document.createElement('div');
                            cFscrNoOrders.setAttribute('id','c_fscr_noorders');
                            loading=document.getElementById('c_fscr_loading');
                            style=loading.style.cssText;

                            cFscrNoOrders.style.cssText=style;
                            cFscrNoOrders.style.display='block';
                            cFscrNoOrders.textContent='You have no pending orders';
                            loading.parentNode.appendChild(cFscrNoOrders);

                        }
                        if(cFscrNoOrders!=null){
                            cFscrNoOrders.style.display='block';
                        }
                    }
                }

            }

            XWF.closewindow();
        }
        else
        {
            //alert('Payment cannot be processed by some reasons');
        }

    }

    /* -----------------------------------------------
     * * * * * * * * * * * * * * * * * * * * * * * * *
             ADDITIONAL FUNCTIONAL FOR ORDERS
                    type = functions
     * * * * * * * * * * * * * * * * * * * * * * * *
    ------------------------------------------------ */
    function cOrderPriceCount(p,t,u,d,dt)
    {
        p=Number(p);

        t=(t==0 || typeof t==='undefined')? 8.875 : Number(t);
        u=(typeof u==='undefined')?0:Number(u);
        d=(typeof d==='undefined')?0:Number(d);
        dt=(typeof dt==='undefined')?0:Number(dt);

        tax=Number(((p/100)*t).toFixed(2));
        taxed=p+tax;
        discount=0;

        if(d>0)
        {
            if(dt==0)
            {
                discount=(p/100)*d;
            }
            else
            {
                discount=d;
            }
            taxed-=discount;
        }

        tip=(p/100)*u;
        total=taxed+tip;

        $cine().order.setNumbers(p,tax,tip,total,discount,t,u,d,dt);
    }
    /* ----------------------------------------------
    * * * * * * * * * * * * * * * * * * * * * * * * *
            USER CREDIT CARD INTERACTIONS
                type = window flow
    * * * * * * * * * * * * * * * * * * * * * * * * *
    ----------------------------------------------- */
    function cineFnUserCardAskFor(){
        var w=cOpenTinyModal({buttonShow:true,buttonName:'Add New Card',buttonEvent:cineAddUserCard,buttonCancel:true});
        h=document.createElement('h2');
        h.style.paddingTop='32px';
        h.innerHTML='You currently have no credit cards associated with your account.';
        w.content.appendChild(h);
    }

    function cineFnUserCardFlow(e,t){

        if(typeof t==='undefined')
            t=false;

        XWF.connQueryObject = {query:"userdata", type:"getusercard"};
        XWF.connQueryData = {
            ident:0,
            data:"",
            ext:t
        };

        $cine().userkey(XWF.connQueryData);

        XWF.send({
            navigationBar:true,
            waitCallback:true,
            raiseWindow:true,
            raiseWindowID:"credit-cards",
            windowClass:'theme',
            onComplete:cineProcUserCard,
            changeTitle:'My Credit Cards'
        });

    }

    function cineAddUserCard(e){

        XWF.connQueryObject = {query:"cardinfo", type:"usertoken"};
        XWF.connQueryData = {
            ident:0, data:""
        };

        $cine().userkey(XWF.connQueryData);

        XWF.send({
            waitCallback:true,
            onComplete:cineAddUserCardBT
        });
    }



    function cineProcUserCard(data){

        var e=data.window;
        var obj=data.content;
        var d=data.data;

        cineDomMutableOne=e;

        var cards = d.result;
        var emty = document.createElement('div');
        emty.setAttribute('id','cinereq_u_pay_cards');
        var k;

        if(d.success){
            tbl=document.createElement('table');
            tbl.className='full-width';
            for(k in cards){
                cur=cards[k];
                if(cards.hasOwnProperty(k)){
                    if(cur['def']==1){
                        defnum=cur['id'];
                    }
                    cineRenderUserCard(tbl,cur['id'],cur['number'],cur['exp'],cur['year'],cur['month'],cur['def']);
                }
                else
                    break;
            }
            emty.appendChild(tbl);
        }

        var btn=document.createElement('button');
        btn.className='cine_fn_color_btn';
        btn.innerHTML='Add New Credit Card';
        btn.addEventListener('click',function(){cineAddUserCard(false)});

        obj.appendChild(emty);
        obj.appendChild(btn);
    }

    function cineRenderUserCard(parent,index,number,exp,year,month,def){

        var elem,out,input,color;

        if(index){
            elem = document.createElement('tr');
            elem.setAttribute('id','cine_usercard_'+index);
            elem.setAttribute('cinecard',index);
            elem.className='cine_list_item cine_list_card';
            color=(def==1)?'active':'';

            elem.addEventListener('click',function(e){cineEditUserCard(e);});

            out='<td> <i class="fa fa-credit-card '+color+'" style="font-size: 20px; padding-left:10px;"></i> </td>';
            out+='<td> XXXX XXXX XXXX '+number+'</td>';
            out+='<td> Expires at: '+exp+'</td>';

            data={cardid:index,number:number,year:year,month:month};
            data=JSON.stringify(data);

            input=document.createElement('input');
            input.setAttribute('id','cine_usercard_'+index+'_i');
            input.setAttribute('type','hidden');
            input.setAttribute('value',data);
            elem.innerHTML = out;
            elem.appendChild(input);
            parent.appendChild(elem);
        }

    }

    function cineAddUserCardBT(data){

        var obj=data.data;

        var w=cOpenTinyModal(false);
        var f=w.content;
        f.innerHTML='<table style="height:100%; width:100%; text-align: center;"><tr><td><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> <br /><h3> Voilà. Dine on the dot. </h3></td></tr></table>';

        if(window.braintree)
            cineAddUserCardDisplay(w,obj);
        else
            $cine().action.cardprocess(w,cineAddUserCardDisplay,obj,false);

    }

    function cineAddUserCardDisplay(e,o){

        var cont=e.content;
        if(o.success) {

            var a = document.createElement('div');
            a.style.cssText = 'width:100%; height:120px; text-align:center; position:absolute; bottom:8px;';

            var s = document.createElement('button');
            s.className = 'cine_fn_color_btn cine_fn_color_btn_good';
            s.innerHTML = 'Add card';
            s.style.cssText = 'width:80%;';
            s.setAttribute('type','submit');
            s.addEventListener('click',function(e){cineBlockButton(e.target);});
            a.appendChild(s);

            var c = document.createElement('button');
            c.className = 'cine_fn_color_btn';
            c.innerHTML = 'Cancel';
            c.style.cssText = 'width:80%;';
            c.addEventListener('click', function (e) {
                this.under.style.display = 'none';
                this.under.parentNode.removeChild(this.under);
                this.modal.parentNode.removeChild(this.modal);
                var i=document.getElementsByTagName('IFRAME');
                var l= i.length;
                for(var k=0;k<l;k++){
                    i[k].parentNode.removeChild(i[k]);
                }
            }.bind(e));
            a.appendChild(c);

            cineFnClearDOM(cont);
            var f = document.createElement('form');
            f.setAttribute('id','cine_braintree_ui');
            f.style.cssText = 'height:100%; width:100%; position:relative;';
            f.innerHTML = '<h3> Credit card </h3>';
            f.appendChild(a);
            cont.appendChild(f);

            braintree.setup(o.result,"dropin",
            {
                container: "cine_braintree_ui",
                paymentMethodNonceReceived:function(event,nonce){

                    XWF.connQueryObject = {query:"cardinfo", type:"addcard"};
                    XWF.connEarlyTermination=false;
                    XWF.connQueryData = {ident:0,data:nonce};

                    $cine().userkey(XWF.connQueryData);

                    XWF.send({
                        waitCallback:true,
                        onComplete:cineAddUserCardFinish
                    });

                }.bind(e)
            });
        }

    }

    function cineAddUserCardFinish(data){

        var obj=data.data;

        if(cineDomMutableOne!=null){
            cineUpdateCardList(obj);
            cRemoveTinyModal();
        }
        else{
            cineFnUserCardFlow(false);
            cRemoveTinyModal();
            cineDomMutableOne=null;
        }

    }

    function cineEditUserCard(e){
        var o,objdata;
        o=cineBoxFnDomObject(e,'TR');
        objdata=document.getElementById(o.id+'_i').value;
        var data=JSON.parse(objdata);

        var w=cOpenTinyModal({buttonShow:true});
        var f=w.content;
        var b=w.button;
        var c=document.createElement('div');
        var h='<h3> Credit card </h3><br /><div style="position: relative;"><div style="position:absolute; color:#FFFFFF; margin-top:78px; width:100%; font-size:20px; text-align:center; ">XXXX XXXX XXXX '+
            data.number+'</div> <i class="fa fa-credit-card" style="font-size: 230px;"></i></div>';

        b.innerHTML='';
        var d=document.createElement('button');
        d.className='cine_fn_color_btn cine_fn_color_btn_dang';
        d.innerHTML='Delete';
        d.style.cssText = 'width:80%;';
        d.addEventListener('click',function(e){cineDeleteUserCard(e,this.window,this.idx)}.bind({window:w,idx:data.cardid}));
        b.appendChild(d);
        d=document.createElement('button');
        d.className='cine_fn_color_btn';
        d.innerHTML='Cancel';
        d.style.cssText = 'width:80%;';
        d.addEventListener('click',function(e){this.under.style.display='none';this.under.parentNode.removeChild(this.under);this.modal.parentNode.removeChild(this.modal);}.bind(w));
        b.style.height = '120px';
        b.appendChild(d);

        c.innerHTML=h;
        f.appendChild(c);

    }

    function cineDeleteUserCard(e,o,id){

        XWF.connQueryObject = {query:"cardinfo", type:"deletecard"};
        XWF.connEarlyTermination=false;
        XWF.connQueryData = {
            ident:id, data:''
        };

        $cine().userkey(XWF.connQueryData);

        XWF.send({
            waitCallback:true,
            onComplete:cineDeleteUserCardAfter
        });

    }

    function cineDeleteUserCardAfter(data){

        var obj = data.data;

        if(obj.success){
            cineUpdateCardList(obj);
            cRemoveTinyModal();
        }

    }

    function cineUpdateCardList(obj){

        var emty = document.getElementById('cinereq_u_pay_cards');
        var defc = document.getElementById('cine_fn_userinfo_card');
        var data = obj;
        var d,k,cur,dnum=false,exp;
        var i=0;

        if(data.result){

            cineFnClearDOM(emty);
            d=data['result'];
            if(d){
                tbl=document.createElement('table');
                tbl.className='full-width';
                for (k in d) {
                    if (d.hasOwnProperty(k)){
                        cur=d[k];
                        cineRenderUserCard(tbl,cur['id'],cur['number'],cur['exp'],cur['year'],cur['month'],cur['def']);
                        if(cur['def']==1){
                            dnum='XXXX XXXX XXXX '+cur['number'];
                            exp=cur['exp'];
                        }
                        i++;

                        if(i>0) {
                            if(defc !== null && dnum)
                                defc.innerHTML = '<table class="full-width height-normal"><tr><td><i class="fa fa-credit-card" style="font-size: 20px; padding-left:10px; padding-right:10px;"></i></td><td> <span id="cineinfo_card_number">'+dnum+'</span></td><td>Expires: '+exp+'</td></tr></table>';
                        } else
                            defc.textContent = 'You have not add credit card yet';
                    }
                    else break;
                }
                if(i==0 && defc!==null){
                    defc.textContent = 'You have not add credit card yet';
                }
                if(emty!=null)
                    emty.appendChild(tbl);
            }

        }

        if(i==0) cineRenderUserCard(emty, false, false, false, false, false, false);

    }


    // MAIN CHECK FOR CART DEFAULT INFORMATION FOR USER
    // THROWS query=restoorder & type=getorderident
    function cineBoxFnOrderCartLoad(o){

        if(o.data)
        {
            var data=o.data;
            //console.log(data);
            if(data.success){
                cineBoxFnRestoOrderPut(o);
            }
            if(data.active){
                cFscrRestoFinderDisplay(false);
                cFnRestoOrderActive(data);
                $cmenu.disable('shopcart');
                $cmenu.enable('orderstat');
            } else {
                cFscrRestoFinderDisplay(true);
                $cmenu.disable('orderstat');
            }

            loading=document.getElementById('c_fscr_loading');
            if(loading!=null){
                loading.style.display='none';
            }
        }

    }

    function cineBoxFnOrderCartLoadSilent(o){

        if(o.data)
        {
            var data=o.data;
            if(data.success){
                $cmenu.enable('shopcart');
                if(cFnRestoCart==null && !cFnRestoCart)
                {
                    cFnRestoCart = data.result;
                    var fc = document.getElementById('cine_fn_base_cart_items');
                    fc.value = JSON.stringify(data.result);
                }
            }
            if(data.active){
                $cmenu.disable('shopcart');
                $cmenu.enable('orderstat');
            } else {
                $cmenu.disable('orderstat');
            }
        }

    }

    // ----------------------------------------------------------------------------------------------------------------
    // SUPPORT FUNCTIONS FOR RESTAURANTS AND RESTAURANTS MENU
    // ----------------------------------------------------------------------------------------------------------------

    function cineBoxFnRestoIPriceCnt(d){
        var p=0,v,vc,vp=0,svp=0;
        p=Number(d.p);
        v=d.s;

        if(v && typeof v==='object'){
            for(var i in v){
                if(v.hasOwnProperty(i)){
                    vc=v[i];
                    if(vc.p>0)
                        vp+=Number(vc.p);
                    if(vc.p2>0)
                        svp+=Number(vc.p2);
                }
            }
        }

        return p+vp+svp;
    }
    // ----------------------------------------
    // ---- Restaurant menu back function -----
    // ----------------------------------------
    Array.prototype.remove = function(from, to) {
        var rest = this.slice((to || from) + 1 || this.length);
        this.length = from < 0 ? this.length + from : from;
        return this.push.apply(this,rest);
    };

    function cineBoxFnRestoMenuBack(e,s,p,c,t){

        if(cineFnCurrentModal!=null){
            cRemoveTinyModal();
        }

        if(!s){

            var n,i;

            if(!p || typeof p!=='object'){
                if(cineFnWindows.length>1){
                    var last = cineFnWindows[cineFnWindows.length - 1];
                    p=document.getElementById(last);
                }else
                    p=document.getElementById('cine_fn_view');
            }

            n=document.createElement('div');
            n.className='cine_fn_resto_menu_b';
            n.setAttribute('cineident',p.id);
            i=document.createElement('div');
            i.className='cine_fn_resto_menu_b_b';
            i.innerHTML = '<div class="c-btn-back"></div>Back';
            i.addEventListener('click',function(e){cineBoxFnRestoMenuBack(e,true,this.p,this.c,this.attr)}.bind({p:p,c:c}));
            n.appendChild(i);

            if(typeof t!=='undefined'){
                i=document.createElement('div');
                i.className='cine_fn_resto_menu_b_t';
                i.innerHTML = t;
                n.appendChild(i);
            }
            cModalFadeOut(p,500);
            cineFnLastWindow=p;
            return n;

        } else {

            if(c)
                cineWinCtrlRemWin(false,c);

            if(p){
                var ttl=p.getAttribute('cinetitle');
                if(ttl!=null){
                    var atl=document.getElementById('cine_fn_ctitle');
                    atl.innerHTML=ttl;
                }
                p.style.display='block';

            }
        }
        return false;
    }

    function cModalFadeOut(m,t){
        var fc=m.firstChild;
        if(fc!=null && cineBoxFnReturnClass(fc.className,'cine_fn_resto_menu_b')) {
            fc.style.opacity = 0;
        }
        var c=m.className;
        if(cineBoxFnReturnClass(c,'container')) t=10;
        //m.className=' slideOutRight';
        setTimeout(function(){this.e.style.display='none';this.e.className=this.c;}.bind({e:m,c:c}),t);
    }
    function cModalFadeOutDestroy(m,t){
        //var c=m.className;
        m.className+=' slideOutRight';
        setTimeout(function(){m.parentNode.removeChild(m);},t);
    }

    function cineWinCtrlRemWin(m,w,c){
        var e,i,r,l,fc;

        if(m && typeof cineFnCurrentWindow!=='undefined'){
            i=cineFnCurrentWindow.getAttribute('modalid');
            if(r=cineFnWindows.indexElm(i)){
                cineDomMutableOne=null;
                e=cineFnCurrentWindow;
                cineFnWindows.remove(r.pos);
                fc= e.firstChild;
                if(fc!=null && cineBoxFnReturnClass(fc.className,'cine_fn_resto_menu_b'))
                    fc.style.opacity=0;
                cModalFadeOutDestroy(e,300);
                l=cineFnWindows.length;
                if(l>0)
                {
                    cineFnCurrentWindow=document.getElementById('cinemodal'+cineFnWindows[l-1]);
                    fc=cineFnCurrentWindow.firstChild;
                    if(fc!=null && cineBoxFnReturnClass(fc.className,'cine_fn_resto_menu_b'))
                        fc.style.opacity=1;
                    cineFnCurrentWindow.style.display='block';
                }
                else
                {
                    p=document.getElementById('cine_fn_view');
                    ttl=p.getAttribute('cinetitle');
                    if(ttl!=null){
                        atl=document.getElementById('cine_fn_ctitle');
                        atl.innerHTML=ttl;
                    }
                    p.style.display='block';
                    cineFnCurrentWindow=undefined;
                }
            }
        }
        if(w){
            i=w.getAttribute('modalid');
            if(r=cineFnWindows.indexElm(i)){
                cineFnWindows.remove(r.pos);
            }
            cineDomMutableOne=null;
            fc= w.firstChild;
            if(fc!=null && cineBoxFnReturnClass(fc.className,'cine_fn_resto_menu_b'))
                fc.style.opacity=0;
            cModalFadeOutDestroy(w,300);
            l=cineFnWindows.length;
            if(l>0){
                cineFnCurrentWindow=document.getElementById('cinemodal'+cineFnWindows[r.pos-1]);
                if(cineFnCurrentWindow!=null) {
                    fc = cineFnCurrentWindow.firstChild;
                    if (fc != null)
                        fc.style.opacity = 1;
                    cineFnCurrentWindow.style.display = 'block';
                }
                else
                {
                    cineFnCurrentWindow=document.getElementById('cinemodal'+cineFnWindows[l-1]);
                    fc=cineFnCurrentWindow.firstChild;
                    if(fc!=null)
                        fc.style.opacity=1;
                    cineFnCurrentWindow.style.display='block';
                }
            }
            else
            {
                p=document.getElementById('cine_fn_view');
                ttl=p.getAttribute('cinetitle');
                if(ttl!=null){
                    atl=document.getElementById('cine_fn_ctitle');
                    atl.innerHTML=ttl;
                }
                p.style.display='block';
                cineFnCurrentWindow=undefined;
            }

        }
    }

    function cineWinCtrlRemAll(){
        var e,i,k,l,d,r;
        l=cineFnWindows.length;
        e=cineFnWindows.slice(0);
        for(i=0;i<l;i++){
            k=document.getElementById('cinemodal'+e[i]);
            if(k!=null){
                if(r=cineFnWindows.indexElm(e[i])){
                    d = k.style.display;
                    if (d != 'none')
                        cModalFadeOutDestroy(k,300);
                    else
                        k.parentNode.removeChild(k);
                    cineFnWindows.remove(r.pos);
                }
            }

            p=document.getElementById('cine_fn_view');
            ttl=p.getAttribute('cinetitle');
            if(ttl!=null){
                atl=document.getElementById('cine_fn_ctitle');
                atl.innerHTML=ttl;
            }
            p.style.display='block';
            cineFnCurrentWindow=undefined;
        }
    }

    function cineBlockButton(e){
        var c;
        if(e!=null){
            c=e.className;
            e.className += ' disabled';
            setTimeout(function(){
                var e=this.o;
                if(e!=null) {
                    e.className = this.c;
                }
            }.bind({o:e,c:c}),1200);
        }
    }

    if(typeof Number.prototype.toRadians == 'undefined')
        {
        Number.prototype.toRadians = function() { return this * Math.PI / 180; };
        }
    function cineBoxFnRealDist(x1,y1,x2,y2){

        x2=Number(x2); y2=Number(y2);
        var r = 6371;
        var l1 = x1.toRadians();
        var l2 = x2.toRadians();
        var dl = (x2-x1).toRadians();
        var dt = (y2-y1).toRadians();
        var a = Math.sin(dl/2) * Math.sin(dl/2) + Math.cos(l1) * Math.cos(l2) * Math.sin(dt/2) * Math.sin(dt/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return (r * c).toFixed(3);
    }

    function cineBoxLabelFrame(o){var e,i,l,k,d;
        e = document.createElement('div'); e.className='cine_fn_domframe';

        if(o.title){
            i = document.createElement('div'); i.className='cine_fn_domframe_l';
            i.textContent=o.title;
            e.appendChild(i);
        }

        i = document.createElement('div'); i.className='cine_fn_domframe_c cine_list_item';
        if(o.color!=undefined){
            i.className = 'cine_fn_domframe_c';
            i.style.backgroundColor = o.color;
        }
        if(o.notpad!=undefined){
            i.className += ' nopadding';
        }
        if(o.hasOwnProperty('elems')){
            d=o.elems;

            if(d.nodeType===1){
                i.appendChild(d);
            }else{
                if(typeof d==='string'){
                    i.insertAdjacentHTML('beforeend', d);
                } else {
                    l = d.length;
                    if (l > 0) {
                        for (k = 0; k < l; k++) {
                            if (typeof d[k] === 'object')
                                i.appendChild(d[k]);
                            else
                                i.insertAdjacentHTML('beforeend', d[k]);
                        }
                    }
                }
            }
        }else{
            i.appendChild(o);
        }
        e.appendChild(i);
        return e;
    }
    function cineBoxFnDomObject(e,type){
        var obj;
        var ev=false;
        if(e.target){
            obj=e.target;
            if(e.preventDefault)
                ev=e;
        }
        else if (typeof e==='string'){
            obj=document.getElementById(e);
        } else
            obj=e;

        if(typeof type !== 'undefined'){
            if(obj.tagName == type)
                return obj;
            else
                return obj.parentNode;
        }
        else
            return {object:obj,event:ev};
    }
    function cineBoxFnModalAfterLoad(o){

        var cc,el,le,i,h=0;

        if(o.nodeType==1) {
            cc = o.getAttribute('modalid');
            cc = document.getElementById('cinecont' + cc);
            el = o.childNodes;
            le = el.length;

            for (i = 0; i < le; i++) {
                if (el[i].className != 'cine_fn_modal_centerer')
                    h += parseInt(el[i].offsetHeight);
            }

            var dh,mw,mh,ms;
            dh = window.innerHeight;
            wh = window.innerWidth;
            fdh = dh + 76;

            mh = o.offsetHeight;
            mh = (mh < fdh) ? (dh - 76) + 'px' : 'auto';
            //o.style.maxWidth = wh + 'px';
            //cc.style.maxWidth = wh + 'px';
            o.style.height = mh;
            cc.style.height = (dh - 76 - h) + 'px';
        }
    }
    function cineCreateDomEl(t,i,c,h,p){

        var e=document.createElement(t);
        e.setAttribute('id',i);
        e.className=c;
        if(h.length>0 && e.nodeName=='INPUT'){
            e.setAttribute('value',h);
        } else {
            e.innerHTML = h;
        }
        if(typeof p==='object'){
            p.appendChild(e);
        }
        return e;

    }
    function cineMakeDays(d,o,id){

        if(typeof o==='undefined')
            o=false;

        var rs;
        var dw=['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
        var dp=[31,28,31,30,31,30,31,31,30,31,30,31];
        var mt=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        var od=['Today','Tomorrow','','','','',''];

        var ud=d.d;
        var um=d.m;
        var uy=d.y;

        var date=new Date();
        var cd=date.getDay();
        var md=date.getDate();
        var ms=date.getMonth();
        var ys=date.getFullYear();

        //if(ud<md){
        //    ud=md;
        //}
        // NEED TO WRITE UP FUNCTION FOR ELIMINATE PAST DAY
        cMonth=ms+1;
        if(um<cMonth || um==cMonth && ud<md){
            ud=md;
        }

        var fd='';
        var nd='';

        var i,k,c,m,y,h,rd,rm,cls;
        k=cd;
        c=md;
        m=ms;
        y=ys;
        h=[];


        for(i=0;i<7;i++){

            if(k>6)
                k=0;

            if(c>dp[m]){
                c=1;
                m++;
            }

            if(c<10)
                rd='0'+c;
            else
                rd=c;

            if(m>12)
                y++;

            rm=m+1;
            if(rm<10)
                rm='0'+rm;

            if(!o){

                if(i>1){
                    h+='<option value="'+y+'-'+rm+'-'+rd+'">'+od[i]+' '+dw[k]+' - '+mt[m]+' '+c+' </option>';
                } else {

                    h+='<option value="'+y+'-'+rm+'-'+rd+'">'+od[i]+' </option>';
                    if(i==0)
                        fd=od[i];
                }

            } else {

                cls='c_vert_datepicker';
                if(i>1){
                    if(c==ud){
                        cls='c_vert_datepicker c_vert_datepicker_a';
                        nd=dw[k]+' - '+mt[m]+' '+c;
                    }
                    rs=document.createElement('div');
                    rs.className=cls;
                    rs.textContent=od[i]+' '+dw[k]+' - '+mt[m]+' '+c;
                    rs.addEventListener('click',function(e){setDatePicker(e.target,this)}.bind({val:y+'-'+rm+'-'+rd,y:y,m:rm,d:rd,ttl:od[i]+' '+dw[k]+' - '+mt[m]+' '+c,idx:id}));
                    h.push(rs);
                } else {
                    if(c==ud){
                        cls='c_vert_datepicker c_vert_datepicker_a';
                        nd=od[i];
                    }
                    rs=document.createElement('div');
                    rs.className=cls;
                    rs.textContent=od[i];
                    rs.addEventListener('click',function(e){setDatePicker(e.target,this)}.bind({val:y+'-'+rm+'-'+rd,y:y,m:rm,d:rd,ttl:od[i],idx:id}));
                    h.push(rs);
                    if(i==0)
                        fd=od[i];
                }
            }

            c++;
            k++;

        }

        ms++;
        if(ms<10)
            ms='0'+ms;
        if(md<10)
            ms='0'+md;
        um=(um<10)?'0'+um:um;
        ud=(ud<10)?'0'+ud:ud;
        return {data:h,year:uy,month:um,day:ud,fday:fd,cday:nd};
    }

    // JSON TEXT CHECK FOR QOUTES
    function cineJsonTextCheck(t){
        var s = {'&':'&amp;','<':' &lt;','>':'&gt; ','\'':'\x27','"':'\\\x22','\\':' '};
        t = t.replace(/[\\\"'&]|(<h3>)|(<\/h3>)|(<)|(>)/ig,function(e){return s[e] || e;});
        t = t.replace(/\r?\n/g,'<br />');
        return t;
    }

    function cineNotifyUser(p){

        var n=document.createElement('div');
        n.className='c_notify_layer';
        n.innerHTML= p.msg;

        document.body.appendChild(n);
        setTimeout(function(){this.parentNode.removeChild(this)}.bind(n),4000);

    }

    function cineNotify(o){

        var l,i,c,b,a;
        if(cineFnNotify!=null){
            l=cineFnNotify;
            if(!document.body.contains(l)){
                l=document.createElement('div');
                l.className='c_notify_layer';
                cineFnNotify=l;
                document.body.appendChild(l);
            }
        } else {
            l=document.createElement('div');
            l.className='c_notify_layer';
            cineFnNotify=l;
            document.body.appendChild(l);
        }

        i=document.createElement('div'); i.className='c_notify_i';

        c=document.createElement('div'); c.className='c_notify_i_c';
        c.innerHTML= o.message;

        b=document.createElement('div'); b.className='c_notify_i_b';
        a=document.createElement('button'); a.className='c_notify_b_b';
        a.innerHTML='<div class="fa fa-times" style="margin-left:-20px"></div>';
        a.addEventListener('click',function(e){cineNotifyClose(e,this)}.bind({notify:l,item:i}));

        if('destroy' in o){
            var d=o['destroy'];
            if(d>0){
                setTimeout(function(e){cineNotifyClose(e,this)}.bind({notify:l,item:i}),d*1000);
            }
        }

        b.appendChild(a);
        i.appendChild(c);
        i.appendChild(b);

        l.appendChild(i);

    }

    function cineNotifyClose(e,o){

        var i = o.item;
        var l = o.notify;
        if(document.body.contains(l)){
            l.removeChild(i);
            var c=l.childNodes;
            c= c.length;
            if(c<1){
                l.parentNode.removeChild(l);
                cineFnNotify=null;
            }
        }

    }

    function cineFnClearDOM(node){
        if(node!=null){
            while (node.firstChild){
                node.removeChild(node.firstChild);
            }
        }
    }

    if(!window.$cine){window.$cine = $cine;}
})();

// ----------------------------------------------------------------
// OUT OF LIBRARY FREE FUNCTIONS -> USAGE WITHOUT NEW CLASS INSTANCE
// -----------------------------------------------------------------
function cOpenTinyModal(s){

    var o=document.createElement('div');
    o.className='fadeInDown';
    var u=document.createElement('div');
    var title=false,ptitle='';

    var h=0;
    if(XWF.windowHeight<=560){
        h=560/100*83;
        sh='position:relative; width:100%; height:'+h+'px; text-align:center;';
        h=XWF.windowHeight-90;
    }
    else{
        h=Math.floor(XWF.windowHeight/100*83);
        sh='position:relative; width:100%; height:100%; text-align:center;';
    }

    o.style.cssText='position:absolute; width:90%; height:'+h+'px; padding:10px; margin:90px 5% 0 5%; top:0; left:0; background:#ffffff; border-radius:8px; overflow-y:scroll';
    u.style.cssText='position:absolute; width:100%; height:'+XWF.windowHeight+'px; top:0; left:0;';
    u.className='c_modal_fancy_bg';

    var f=document.createElement('div');
    f.style.cssText=sh;
    o.appendChild(f);

    if(s.appTitle)
    {
        title=true;
        //atl=document.getElementById('cine_fn_ctitle');
        var titleLayer=document.createElement('div');
        titleLayer.style.cssText = 'position: absolute; top:34px; left:0;';
        titleLayer.innerHTML='<span id="cine_fn_ctitle" class="navbar-brand" style="max-width:220px; position: relative; overflow: hidden; line-height:32px; margin-top: -6px;"> '+s.appTitle+' </span>';
        u.appendChild(titleLayer);
        //ptitle=atl.innerHTML;
        //atl.innerHTML=s.appTitle;
    }

    var inititator=null;

    if(s.initiator){
        inititator= s.initiator;
    }

    var r={modal:o,under:u,content:f,title:ptitle,titleChange:title,initiator:inititator};

    if(s.buttonShow)
    {
        c = document.createElement('div');
        c.style.cssText = 'width:100%; height:50px; text-align:center; position:absolute; bottom:8px;';

        if(s.hasOwnProperty('buttonName')) {

            closeWindow=true;
            if(s.hasOwnProperty('buttonCloseWindow')) {
                closeWindow= s.buttonCloseWindow;
            }

            b = document.createElement('button');
            b.className = "cine_fn_color_btn cine_short_btn";
            b.style.cssText = 'width:90% !important;';
            b.innerHTML = s.buttonName;
            b.addEventListener('click', function (e) {

                if (typeof s.buttonEvent === 'function') {
                    s.buttonEvent(e);
                }
                //if (title) {
                //    atl = document.getElementById('cine_fn_ctitle');
                //    atl.innerHTML = this.title;
                //}

                if(closeWindow)
                {
                    this.under.style.display = 'none';
                    this.under.parentNode.removeChild(this.under);
                    this.modal.parentNode.removeChild(this.modal);
                    cineFnCurrentModal = null;
                }

            }.bind(r));
            c.appendChild(b);
            r['buttonNormal'] = b;

        }

        if(s.hasOwnProperty('buttonCancel')){
            c.style.height="96px";
            b=document.createElement('button');
            b.className="cine_fn_color_btn cine_short_btn";
            b.style.cssText='width:90% !important; margin-top:0px !important;';
            b.innerHTML= 'Cancel';
            b.addEventListener('click',function(e){

                if(title){
                    atl=document.getElementById('cine_fn_ctitle');
                    atl.innerHTML=this.title;
                }
                this.under.style.display='none';
                this.under.parentNode.removeChild(this.under);
                this.modal.parentNode.removeChild(this.modal);
                cineFnCurrentModal=null;

            }.bind(r));
            c.appendChild(b);
            r['buttonCancel']=b;
        }

        f.appendChild(c);
        r['button']=c;
    }

    document.body.appendChild(u);
    document.body.appendChild(o);
    cineFnCurrentModal=r;
    return r;
}

function cRemoveTinyModal()
{
    if(cineFnCurrentModal!=null && document.contains(cineFnCurrentModal.modal)){

        if(cineFnCurrentModal.titleChange){
            atl=document.getElementById('cine_fn_ctitle');
            atl.innerHTML=cineFnCurrentModal.title;
        }
        cineFnCurrentModal.modal.parentNode.removeChild(cineFnCurrentModal.modal);
        cineFnCurrentModal.under.parentNode.removeChild(cineFnCurrentModal.under);

        cineFnCurrentModal=null;
    }
}

function cTimeObject(s){
    if(typeof s!=='undefined'){
        var e=s.split(' ');
        var d=e[0];
        var t=e[1];
        if(typeof d!=='undefined')
            d=d.split('-');
        else
            d=['0000','00','00'];
        if(typeof t!=='undefined') {
            t = t.split(':');
            t[2]=(typeof t[2]==='undefined')?'00':t[2];
        }
        else
            t=['00','00','00'];
        tMin=Number(t[1]);
        tHor=Number(t[0]);
        tSec=Number(t[2]);
        dDay=Number(d[2]);
        dMon=Number(d[1]);
        return {year:d[0],month:(dMon<10)?'0'+dMon:dMon,day:(dDay<10)?'0'+dDay:dDay,hour:(tHor<10)?'0'+tHor:tHor,minute:(tMin<10)?'0'+tMin:tMin,second:(tSec<10)?'0'+tSec:tSec,
            y:Number(d[0]),m:dMon,d:dDay,h:tHor,i:tMin,s:tSec};
    }
}
function cineTimeNormalize(h,m){
    var nh=h%12;
    if(nh==0)nh=12;
    if(typeof m==='string')
        return nh+":"+m+(h<12?' am':' pm');
    else
        return nh+":"+(m<10?"0"+m:m)+(h<12?' am':' pm');
}
function cGetSetDateToOrder(e){
    if(cineFnOrderObject!=null){

        XWF.connQueryObject = {query:"restoorder", type:"putordertime"};
        XWF.connQueryData = {
            ident:cineFnOrderObject.id,
            data:cineFnOrderObject.year+'-'+cineFnOrderObject.month+'-'+cineFnOrderObject.day+' '+cineFnOrderObject.hour+':'+cineFnOrderObject.minute+':00',
            ext:cineFnOrderObject.party,
            adata:cineFnOrderObject.key
        };

        $cine().userkey(XWF.connQueryData);

        XWF.send({
            waitCallback:true
        });
    }
}
function cGetSetDate(id,func){

    var device=(cineDevice=='ios');
    var execFunc=false;
    if(typeof id==='undefined' || !id){
        id='';
    }

    if(typeof func==='function'){
        execFunc=true;
    }

    var o;

    if(id.length>0){
        o=cOpenTinyModal({buttonShow:true,buttonName:'Set Dining Time',buttonEvent:cGetSetDateToOrder,appTitle:'Reservation time'});
    }
    else if(execFunc){
        o=cOpenTinyModal({buttonShow:true,buttonName:'Set Dining Time',buttonEvent:function(){func();},appTitle:'Reservation time'});
    }
    else {
        o=cOpenTinyModal({buttonShow:true,buttonName:'Set Dining Time',appTitle:'Reservation time'});
    }

    var dp=document.createElement('div');
    dp.className='select-datepicker';
    o.content.appendChild(dp);

    var ct=document.createElement('div');
    ct.className='getsetdate-ctrl';
    var tp=document.createElement('div');
    tp.className='c_time c_time_m';
    ct.appendChild(tp);

    if(cineDevice=='ios'){
        tp=document.createElement('input');
        tp.setAttribute('type','time');
        tp.className='input-small select-timepicker';
        tp.style.cssText='border:none; height:36px; background:none; text-align:center;';
        tp.setAttribute('placeholder','hh:mm');
        tp.addEventListener('change',function(e){setTimePicker(e.target,this)}.bind(id));
        ct.appendChild(tp);
    } else {
        tp=document.createElement('div');
        tp.className='input-small select-timepicker';
        tp.style.cssText='border:none; height:36px; background:none; text-align:center; padding-top:6px; padding-right:16px;';
        //tp.setAttribute('id','c_time'+id);
    }

    ct.appendChild(tp);
    o.content.appendChild(ct);

    ct=document.createElement('div');
    ct.className='getsetdate-ctrl';
    var pp=document.createElement('div');
    pp.className='c_party c_party_m';
    ct.appendChild(pp);
    pp=document.createElement('select');
    pp.className='c_fn_select';
    pp.innerHTML='<option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option>';
    pp.addEventListener('change',function(e){setPartyPicker(e.target,this)}.bind(id));

    ct.appendChild(pp);
    o.content.appendChild(ct);

    var sft,sfd,sfp,ti;
    if(id.length<1){
        sft=null;
        var to=cineFnTimeObject;
        ti = cTimeObject(to.year+'-'+to.month+'-'+to.day);
        if(device)
            tp.value=to.hour+':'+to.minute+':00';
        else
            tp.innerHTML=cineTimeNormalize(Number(to.hour),Number(to.minute));
        pp.value=to.party;
    } else {
        sft=true;
        ti = cTimeObject(cineFnOrderObject['year']+'-'+cineFnOrderObject['month']+'-'+cineFnOrderObject['day']);
        if(device)
            tp.value=cineFnOrderObject['hour']+':'+cineFnOrderObject['minute']+':00';
        else
            tp.innerHTML=cineTimeNormalize(Number(cineFnOrderObject['hour']),Number(cineFnOrderObject['minute']));

        pp.value=cineFnOrderObject.party;
    }

    var sel=$cine().helper.timeToSelect(ti,true,id);
    selt=sel.data;

    for(var i=0;i<selt.length;i++){
        dp.appendChild(selt[i]);
    }

    if(!device){
        tp.addEventListener('click', function (e) {
            t=this.obj;
            time = '';
            if (t == null)
                time = cineFnTimeObject.hour + ':' + cineFnTimeObject.minute;
            else
                time = cineFnOrderObject.hour + ':' + cineFnOrderObject.minute;

            cAddTimeSelect(time, t, document.getElementById('c_time' + this.id), this.fld);
        }.bind({id: id, obj: sft, fld: tp}));
    }

}

function setDatePicker(e,o){
    v=o.val;
    t=o.ttl;
    id=o.idx;

    if(id.length<1) {
        id = '';
        cineFnTimeObject.year = o.y;
        cineFnTimeObject.month = o.m;
        cineFnTimeObject.day = o.d;
        cineFnTimeObject.curday = o.ttl;
    }
    else{

        cineFnOrderObject['year']=o.y;
        cineFnOrderObject['month']=o.m;
        cineFnOrderObject['day']=o.d;
    }

    var cdate=document.getElementById('c_date'+id);
    if(cdate!=null)
        cdate.textContent=t;

    var nodes= e.parentNode.childNodes;
    for(var i=0;i<nodes.length;i++){
        c=nodes[i];
        if(c.className=='c_vert_datepicker c_vert_datepicker_a'){
            c.className='c_vert_datepicker';
        }
    }
    e.className='c_vert_datepicker c_vert_datepicker_a';
}

function setTimePicker(e,i){

    var tm=e.value.split(':');

    if(i.length<1){
        cineFnTimeObject.hour=tm[0];
        cineFnTimeObject.minute=tm[1];
        cineFnTimeObject.second='00';
    } else {
        cineFnOrderObject['hour']=tm[0];
        cineFnOrderObject['minute']=tm[1];
        cineFnOrderObject['second']='00';
    }

    var hr=parseInt(tm[0]);
    var m=parseInt(tm[1]);
    if(hr>=24)
        hr=hr-24;
    var cdate=document.getElementById('c_time'+i);
    if(cdate!=null)
        cdate.textContent=cineTimeNormalize(hr,m);
}

function setPartyPicker(e,i){
    var v=parseInt(e.value);
    if(i.length<1){
        cineFnTimeObject.party=v;
    } else {
        cineFnOrderObject['party']=v;
    }
    var cdate=document.getElementById('c_party'+i);
    if(cdate!=null)
        cdate.textContent=v;
}

var cTouches={};
var timeH=0;
var tCursorW=0;
var tCursorM=0;
var tCursorDx=0;
var tCursorH=0;
var tCursorL=0;
var tCursorDy=0;
var timeM=0;
var timeAMPM;
var timeAMPMElem;
var timeAMPMDisp=['AM','PM'];

function cAddTimeSelect(time,obj,vis,fld){

    time=time.split(':');
    timeH=Number(time[0]);
    timeM=Number(time[1]);
    if(timeH>=12)
        timeAMPM=1;
    else
        timeAMPM=0;

    var c=document.createElement('div');
    c.className="c_xtime";
    c.style.cssText="position:absolute !important; width:100%; height:100%; background:#333333; top:0; left:0; text-align:center;";
    // BUTTONS
    var rb=document.createElement('div');
    rb.style.cssText="position:relative; display:block; width:100%; height:48px; margin-top:8px;";
    // button SetClock
    var b=document.createElement('button');
    b.className='cine_fn_color_btn';
    b.innerHTML='Done';
    b.addEventListener('click',function(){
        this.parentNode.removeChild(this);
        if(timeAMPM==1){
            timeH+=12;
            if(timeH==24){
                timeH=12;
            }
        } else {
            (timeH==12)?timeH=0:timeH;
        }
        if(obj!=null){
            //obj.value=((timeH<10)?'0'+timeH:timeH)+':'+((timeM<10)?'0'+timeM:timeM);
            cineFnOrderObject['hour']=(timeH<10)?'0'+timeH:timeH;
            cineFnOrderObject['minute']=(timeM<10)?'0'+timeM:timeM;
        }
        else
        {
            cineFnTimeObject.hour=(timeH<10)?'0'+timeH:timeH;
            cineFnTimeObject.minute=(timeM<10)?'0'+timeM:timeM;
            cineFnTimeObject.h=timeH;
            cineFnTimeObject.m=timeM;
        }

        if(vis != null)
            vis.textContent=cineTimeNormalize(timeH,timeM);

        if(typeof fld!=='undefined'){
            //fld.value=cineTimeNormalize(timeH,timeM);
            fld.innerHTML=cineTimeNormalize(timeH,timeM);
        }

        cTouches={};
        timeAMPMElem=null;
    }.bind(c));
    rb.appendChild(b);
    // button Cancel
    b=document.createElement('button');
    b.className='cine_fn_color_btn';
    b.innerHTML='Cancel';
    b.addEventListener('click',function(){
        this.parentNode.removeChild(this);
    }.bind(c));
    rb.appendChild(b);

    var r=document.createElement('div');
    r.className='c_vtime_c';
    c.appendChild(r);

    var t=document.createElement('div');
    t.className='c_vtime_l';
    r.appendChild(t);
    t=document.createElement('div');
    t.className='c_vtime_r';
    r.appendChild(t);

    var h=document.createElement('div');
    h.className='c_vtime_h';

    var m=document.createElement('div');
    m.className='c_vtime_m';

    var d=document.createElement('div');
    d.className='c_vtime_d';
    d.textContent=timeAMPMDisp[timeAMPM];
    d.addEventListener('click',function(){cTimeAmPm(true);});
    timeAMPMElem=d;
    c.appendChild(d);

    c.appendChild(rb);

    var i,el,k;

    timeH=timeH%12;
    if(timeH==0)
        timeH=12;
    k=timeH-2;
    if(k<1)
        k=12+k;
    for(i=1;i<=5;i++){
        if(k>12){
            k=1;
        }
        el=document.createElement('div');
        el.textContent=k;
        el.className='c_vtime'+i;
        h.appendChild(el);
        k++;
    }

    k=timeM-10;
    if(k<0)
        k=60+k;
    lo=k%5;
    if(lo>0){
        k=k-lo;
    }
    timeM=k+10;
    for(i=1;i<=5;i++){
        if(k>=60)
            k=0;
        el=document.createElement('div');
        el.className='c_vtime'+i;
        (k<10)?el.textContent='0'+k:el.textContent=k;
        m.appendChild(el);
        k+=5;
    }

    timeM=(timeM>59)?timeM-60:timeM;

    h.addEventListener("touchstart",function(e){cTimeTouch(e)},false);
    h.addEventListener("touchmove",function(e){cTimeTouchMove(e,this,12)}.bind(h),false);
    h.addEventListener("touchend",function(e){tCursorDy=0;tCursorH=0;tCursorL=0},false);
    m.addEventListener("touchstart",function(e){cTimeTouch(e)},false);
    m.addEventListener("touchmove",function(e){cMinuteTouchMove(e,this,60)}.bind(m),false);
    m.addEventListener("touchend",function(e){tCursorDy=0;tCursorH=0;tCursorL=0},false);
    r.appendChild(h);
    r.appendChild(m);

    document.body.appendChild(c);
}

function cTimeTouch(e){
    var touches = e.changedTouches;
    e.preventDefault();
    for(var t=0;t<touches.length;t++){
        cTouches[touches[t].identifier]={
            pageY:touches[t].pageY
        };
    }
}
function cTimeTouchMove(e,o,s){
    var touches = e.changedTouches;
    e.preventDefault();
    var dy=0,ny=0,y=0;
    var time;
    for(var t=0;t<touches.length;t++){
        var theTouchInfo=cTouches[touches[t].identifier];
        y=theTouchInfo.pageY;
        ny=touches[t].pageY;
        dy=Math.abs(ny-y);
    }
    tCursorH+=dy-tCursorL;
    tCursorL=dy;
    time=timeH;

    if(Math.abs(tCursorH)>40){
        var c=o.childNodes;
        if(tCursorDy>ny)
            time=cTimeHFwd(c,time,s);
        else
            time=cTimeHBkwd(c,time,s);
        tCursorH=0;
        timeH=time;
        cTimeAmPm();
    }
    tCursorDy=ny;
}
function cMinuteTouchMove(e,o,s){
    var touches = e.changedTouches;
    e.preventDefault();
    var dy=0,ny=0,y=0;
    var time;
    for(var t=0;t<touches.length;t++){
        var theTouchInfo=cTouches[touches[t].identifier];
        y=theTouchInfo.pageY;
        ny=touches[t].pageY;
        dy=Math.abs(ny-y);
    }
    tCursorH+=dy-tCursorL;
    tCursorL=dy;
    time=timeM;
    if(Math.abs(tCursorH)>40){
        var c=o.childNodes;
        if(tCursorDy>ny)
            time=cTimeMFwd(c,time,s);
        else
            time=cTimeMBkwd(c,time,s);
        tCursorH=0;
        timeM=time;
    }
    tCursorDy=ny;
}
function cTimeMFwd(o,t,s){
    var k,i;
    t+=5;
    if(t>=s)
        t=0;
    k=t-(5*2);
    for(i=0;i<5;i++){
        if(k>=s)k=0;
        if(k<0)k=s+k;
        (k<10)?o[i].textContent='0'+k:o[i].textContent=k;
        k+=5;
    } return t;

}
function cTimeMBkwd(o,t,s){
    var k,i;
    t-=5;
    if(t<0)
        t=s;
    k=t-(5*2);
    for(i=0;i<5;i++){
        if(k<0)k=s+k;
        if(k>=s)k=0;
        (k<10)?o[i].textContent='0'+k:o[i].textContent=k;
        k+=5;
    } return t;

}
function cTimeHFwd(o,t,s) {
    var k,i;
    t++;
    if (t > s)
        t = 1;
    k = t - 2;
    for (i = 0; i < 5; i++) {
        if (k > s)
            k = 1;
        if (k < 1)
            k = s + k;
        o[i].textContent = k;
        k++;
    } return t;
}
function cTimeHBkwd(o,t,s) {
    var k, i;
    t--;
    if (t < 1)
        t = s;
    k = t - 2;
    for (i = 0; i < 5; i++) {
        if (k <= 0)
            k = s + k;
        if (k > s)
            k = 1;
        o[i].innerHTML = k;
        k++;
    } return t;
}

function cTimeAmPm(s){
    if(typeof s==='undefined'){
        if(timeH==12){
            (timeAMPM==0)?timeAMPM=1:timeAMPM=0; timeAMPMElem.textContent=timeAMPMDisp[timeAMPM];
        }
    } else {
        (timeAMPM==0)?timeAMPM=1:timeAMPM=0; timeAMPMElem.textContent=timeAMPMDisp[timeAMPM];
    }
}

// **********************************************************
// ----------       FIRST SCREEN FUNCTIONS   ----------------
// **********************************************************
function cineResetRestaurant(e){

    cFscrHeader.style.display='block';
    if(cFscrRlabel!=null){
        cFscrRlabel.parentNode.removeChild(cFscrRlabel);
        cFscrRlabel=null;
    }
    if(cFscrTlabel!=null){
        cFscrTlabel.parentNode.removeChild(cFscrTlabel);
        cFscrTlabel=null;
    }
    if(cFscrMbutton!=null){
        cFscrMbutton.parentNode.removeChild(cFscrMbutton);
        cFscrMbutton=null;
    }
    cFscrReservation.style.display='none';
    cFscrRbutton.style.display='block';
    cFscrRestaurant=null;
}
function cFscrRestoFinderDisplay(s){
    if(s){
        cFscrHeader.style.display='block';
        cFscrRbutton.style.display='block';
    } else {
        cFscrHeader.style.display='none';
        cFscrRbutton.style.display='none';
    }
}

//window.onresize = function () {
    //cResizeView(window.innerWidth,window.innerHeight);
//};

// WINDOWS ROTATING FUNCTION
function cResizeView(w,h){

    cineViewW=XWF.windowWidth;
    cineViewH=XWF.windowHeight;
    var max=Math.max(cineViewH,cineViewW);

    // For Any Windows and views in system do resize
    var v=document.getElementsByClassName('xxxview');
    var l=v.length;
    var c,top;

    for(i=0;i<l;i++)
    {

        if(v.hasOwnProperty(i)){
            c=v[i];
            top=c.style.top;
            if(top.length>0)
                top=$cine().helper.styleToPx(top);

            if(top>0) {
                c.style.height=(cineViewH-top)+'px';
                c.style.width=cineViewW+'px';
                $cine().modal.setModalLoaded(c);
            } else {
                c.style.height=cineViewH+'px';
                c.style.width=cineViewW+'px';
            }

        }

    }

    // If tiny Modal is onscreen
    if(cineFnCurrentModal!=null)
    {
        var u=cineFnCurrentModal.under;
        var m=cineFnCurrentModal.modal;
        var n=cineFnCurrentModal.content;
        u.style.height=cineViewH+'px';
        // Modal -100px -> 90 offset Top + 10 offset bottom
        m.style.height=cineViewH-100+'px';
        u.style.width=cineViewW+'px';
        m.style.width=((cineViewW/100))*90+'px';
        // Inner content -100px of modal
        // plus -20px of paddings
        n.style.height=(max-120)+'px';

    }

    // If FirstScreenBlock is onscreen
    if(cFscrBox!=null){

        if(cineViewH < 400){
            //var mTop=$cine().helper.styleToPx(cFscrBox.style.marginTop);
            //mTop-=mTop/3;
            //cFscrBox.style.marginTop=mTop+'px';
        } else {
            //cFscrBox.style.marginTop=-185+'px';
        }

    }

}

XWF.connection({
    server:"https://api.myvoila.me/srv",
    //server:'http://eatnow1212/srv',
    serverMethod:"url",
    eventOnError:$cine().error,
    queryObject:{
        query:"restoorder", type:"getorderident"
    },
    queryData:{
        user:0, userkey:"", ident:0
    }
});

var clearServer=XWF.connServer.replace('srv','');
var appImagePath=clearServer+'img/';